
	CC=sdcc.exe

ifeq ($(wildcard $(addsuffix /rm,$(subst :, ,$(PATH)))),)
	LD= sdld.exe
	AS= sdas326.exe
	RM= rm
	H2B= hex2bin.exe

else
	LD=sdld
	AS=sdas326
	RM=rm
	H2B=hex2bin

endif


 bins= ms233a7196_dmi_2spk.bin \
ms233a7196pa345_dmi_2spk.bin \
ms233a7196_dmi_1spk.bin ms233a7196pa345_dmi_1spk.bin \
ms233a7196_dmi_1spk_1mbps.bin ms233a7196pa345_dmi_1spk_1mbps.bin \

#bins = ms233a7196_dmi_1spk.bin ms233a7196pa345_dmi_1spk.bin


NOW := "\"$(shell cmd /C date /T) $(shell cmd /C time /T)\""

all: $(bins) md5list.txt

md5list.txt: $(bins)
	echo /U ---$(NOW)--- >> $@
	md5sum -b $(bins) >> $@
	echo  /U ---$(NOW)--- >> $@


%.bin: %.ihx spi1.bin1
	$(H2B) $<
	cat $@ zero64k.bi1 > aa1.bin
	head -c 16384 aa1.bin > aa2.bin
	cat aa2.bin spi1.bin1 > $@
	$(RM) aa1.bin
	$(RM) aa2.bin
	copy $@ z:\

%.ihx: %.rel _ms326_cstartup.rel api_stdbymode.rel api_play_job.rel
	$(LD) -u -m -i -y $@ $^ -l ms326sdcc.lib -l ms326sph.lib


ms233a7196_dmi_1spk_1mbps.rel: ms233a7196.c  spi1.h ms233def.h Makefile qechk.s
	echo	.define DMI 1 > dmi.def
	$(CC) -pMS326 -DMBPS1=1 -DDMI=1 -DHANDSETPA7=0 -c $< -o $@ 

ms233a7196pa345_dmi_1spk_1mbps.rel: ms233a7196.c  spi1.h ms233def.h Makefile qechk.s
	echo	.define DMI 1 > dmi.def
	$(CC) -pMS326 -DMBPS1=1 -DDMI=1 -DHANDSETPA7=0 -DPAKEY345=1 -c $< -o $@ 


ms233a7196_1spk.rel: ms233a7196.c  spi1.h ms233def.h Makefile qechk.s
	echo	.define DMI0 > dmi.def
	$(CC) -pMS326 -DDMI=0 -DHANDSETPA7=0 -c $< -o $@ 

ms233a7196_1spksim.rel: ms233a7196.c  spi1.h ms233def.h Makefile qechk.s
	echo   .define DMI0 > dmi.def
	$(CC) -pMS326 -DSIMULATION=1 -DDMI=0 -DHANDSETPA7=0 -c $< -o $@ 

ms233a7196_dmi_1spksim.rel: ms233a7196.c  spi1.h ms233def.h Makefile qechk.s
	echo   .define DMI 1 > dmi.def
	$(CC) -pMS326 -DSIMULATION=1 -DDMI=1 -DHANDSETPA7=0 -c $< -o $@ 


ms233a7196_2spk.rel: ms233a7196.c  spi1.h ms233def.h Makefile qechk.s
	echo  .define DMI0 > dmi.def
	$(CC) -pMS326 -DDMI=0 -DHANDSETPA7=1 -c $< -o $@ 

ms233a7196_2spksim.rel: ms233a7196.c  spi1.h ms233def.h Makefile qechk.s
	echo  .define DMI0 > dmi.def
	$(CC) -pMS326 -DSIMULATION=1 -DDMI=0 -DHANDSETPA7=1 -c $< -o $@ 

ms233a7196_dmi_2spk.rel: ms233a7196.c  spi1.h ms233def.h Makefile qechk.s
	echo  .define DMI 1 > dmi.def
	$(CC) -pMS326 -DDMI=1 -DHANDSETPA7=1 -c $< -o $@ 


ms233a7196pa345_dmi_2spk.rel: ms233a7196.c  spi1.h ms233def.h Makefile qechk.s
	echo  .define DMI 1 > dmi.def
	$(CC) -pMS326 -DDMI=1 -DHANDSETPA7=1 -DPAKEY345=1 -c $< -o $@ 

ms233a7196_dmi_1spk_sim.rel: ms233a7196.c  spi1.h ms233def.h Makefile qechk.s
	echo  .define DMI 1 > dmi.def
	$(CC) -pMS326 -DDMI=1 -DHANDSETPA7=0 -DSIMULATION=1 -c $< -o $@ 

ms233a7196_dmi_1spk.rel: ms233a7196.c  spi1.h ms233def.h Makefile qechk.s
	echo  .define DMI 1 > dmi.def
	$(CC) -pMS326 -DDMI=1 -DHANDSETPA7=0 -c $< -o $@ 


ms233a7196pa345_dmi_1spk.rel: ms233a7196.c  spi1.h ms233def.h Makefile qechk.s
	echo  .define DMI 1 > dmi.def
	$(CC) -pMS326 -DDMI=1 -DHANDSETPA7=0 -DPAKEY345=1 -c $< -o $@ 



ms233a7196pa345_1spk.rel: ms233a7196.c  spi1.h ms233def.h Makefile qechk.s
	echo  .define DMI0 > dmi.def
	$(CC) -pMS326 -DPAKEY345=1 -DDMI=0 -DHANDSETPA7=0 -c $< -o $@ 

ms233a7196pa345_2spk.rel: ms233a7196.c  spi1.h ms233def.h Makefile qechk.s
	echo  .define DMI0 > dmi.def
	$(CC) -pMS326 -DPAKEY345=1 -DDMI=0 -DHANDSETPA7=1 -c $< -o $@ 


ms233a7196_dmisim.rel: ms233a7196.c  spi1.h ms233def.h Makefile qechk.s
	echo  .define DMI 1 > dmi.def
	$(CC) -pMS326 -DSIMULATION=1 -DDMI=1 -DHANDSETPA7=1 -c $< -o $@ 

_ms326_cstartup.rel: _ms326_cstartup.asm
	$(AS) -l -s -o -y _ms326_cstartup.asm

%.rel: %.c
	$(CC) -pMS326 -c $<

clean:
	$(RM) -rf ms233a7196.tsk
	$(RM) -rf ms233a7196.asm
	$(RM) -rf *.cdb
	$(RM) -rf *.lst
	$(RM) -rf *.sym
	$(RM) -rf *.mp3
	$(RM) -rf *.rel
	$(RM) -rf *.rst
	$(RM) -rf *.ihx
	$(RM) -rf *~


C_SRC = ms326a7137.c 
ASM_SRC = 
SRCDIRS =.
PRJ = a7137
LIBS1 = MS326sdcc.lib
LIBS2 = ms326sph.lib
TARGET = MS326
OBJDIR = obj
OUTDIR = bin
AOPT1 = 
COPT1 = 
LOPT1 = 
AUD = spi.bin
AUDOFF = 0x003000
#following no change

ASMBASE=$(basename $(notdir $(ASM_SRC)))
CBASE=$(basename $(notdir $(C_SRC)))
COBJS=$(addprefix $(OBJDIR)/, $(addsuffix .rel,$(CBASE)))
ASMOBJS=$(addprefix $(OBJDIR)/, $(addsuffix .rel,$(ASMBASE)))
ASMOFC=$(addsuffix .asm, $(basename $(C_SRC)))

OBJS= $(COBJS) $(ASMOBJS)
COPT = -p$(TARGET)

.PHONY: clean

ALL:$(OUTDIR)/$(PRJ).bin
	
	

#only C obj has .d

-include $(OBJS:.rel=.d)

VPATH=$(SRCDIRS)

$(OUTDIR)/$(PRJ).bin: $(OUTDIR)/$(PRJ).ihx $(AUD)
	hex2bin $(OUTDIR)/$(PRJ).ihx
	mergeBin -f $(OUTDIR)/$(PRJ).bin 0 -f $(AUD) $(AUDOFF) -o $(OUTDIR)/$(PRJ).bin


$(OUTDIR)/%.ihx: $(OBJS)
	sdld -u -m -i -y $(LOPT1) $(OUTDIR)/$(PRJ) $^ -l $(LIBS1) -l $(LIBS2)
	hex2bin $@

$(OBJDIR)/%.rel: %.asm
	sdas326 -l -s -y $(AOPT1) -dep $(basename $@).d -o $@ $<

$(OBJDIR)/%.rel: %.c
	sdcc $(COPT) $(COPT1) -c $< -o $@
	sdcc $(COPT) $(COPT1) -E -Wp-MT -Wp$(basename $@).rel -MM $< > $(basename $@).d

clean:
	del /Q $(ASMOFC) $(OBJDIR)\*.d $(OBJDIR)\*.rel $(OBJDIR)\*.asm $(OBJDIR)\*.lst $(OBJDIR)\*.sym $(OUTDIR)\$(PRJ).ihx $(OUTDIR)\$(PRJ).bin 
	


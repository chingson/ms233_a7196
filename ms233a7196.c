
#include <string.h>
#include <ms326.h>
#include <ms326sphlib.h>
#include <setjmp.h>
#include "ms233def.h"

#ifndef __SDCC
#define __critical
#endif
//#include "A7196/define.h"
#include "a7196/A7196reg.h"
#include "spi1.h"

typedef unsigned char uChar;

#ifndef HANDSETPA7
#error("HANDSETPA7 not defined")
#endif
#ifndef DMI
#error("DMI NOT DEFINED")
#endif

//#define DEBUG_RING

//2018 Nov,
// try to add multi-talk support

// basic the method is to seperate dialing channel and talk channel
// when dialing, the caller should check the channel of lowest RSSI
// using channel:
// dialing: 149 154
// talk1: 161 1

// 2014 Jun, MS233 B version
// use the external oscillator from RF module
// we can geneTRTR timing correctly
// CE pin change from ROSC to PA0!!
// because ROSC need to be clock input

// 2017 DEC, port to MS326

// loudest handset
#define HANDSETVOL_DEFAULT 0

// reg37 is channgel
#define REG37_DEFAULT 37
#define RFANA_DEFAULT 0x10
#define REG3E_DEFAULT 0

// we define 0x34~0x37 are voice tags
#define TAG_VOICE_CHK 0x34
#define TAG_VOICE_MASK 0xFC
#define TAG_VOICE_LOUDNESS 0x03
// middle loudness

#define TAG_DATA 0x9a
#define TAG_HANGUP 0x57

typedef uChar Uint8;

uChar myId;

//void dbg_uChar(uChar dd);

void delay2nms(uChar);
void rf_reset(void);
void rf_cal(void);
void sw_to_osc(void);

void gen_default_chan(void);
uChar conv2manch(uChar);
void set_rf_id(void);
void init(void);
uChar get_key(void);

uChar SPI_Read_Reg(uChar addr);

//

void SPI_Read_Buf_PL(uChar addr); // input is ROMPTR!!
#if (SPIMACRO)
//void SPI_Write_Reg(uChar reg, uChar value);
#define SPI_Write_Reg(a, b) \
    {                       \
        cslow();            \
        SPIDAT = a;         \
        SPIDAT = b;         \
        cshigh();           \
    }
#define SPI_Write_Strobe(x) \
    {                       \
        cslow();            \
        SPIDAT = x;         \
        cshigh();           \
    }

#else
void SPI_Write_Reg(uChar reg, uChar value);
void SPI_Write_Strobe(uChar cmd);

#endif

void SPI_Write_Reg_Page(uChar reg, uChar value, uChar page);

void SPI_Write_Buf_RAM(uChar add); // MSB of n is ROM
void flushtr(void);
void sleep(uChar singlepinwk); // pass 1 if using PA to wake up
uChar read_packet(uChar purge);
uChar read_voice_packet(void);
void enter_ringing_mode(void);

void enter_idle_mode(void);
void answer_call(uChar master); // 1 means I am the caller
void number_key(void);
void beep_short(uChar period);
// in assembly qechk.s
void prepare_voice_tx(void);
void shift_voice_buf(void);
void adjHeadNow(void);
// above 3 functions
uChar out_packet(uChar change_channel); // return value if key checked, 1 means hangup
void warmRst(void);
void set_new_addr(void);
void vol_up(void);
void vol_dn(void);
void beep2ifNotIdle(void); // beep 2 sounds
//uChar check_mem(void);
//void backup_ram(void);
//void cal_sum(void);
//void set_specialf(uChar);
static uChar getpbcode(uChar v);
// backup part
uChar SENDTAG;
uChar RX0_Address[5]; // Default 0,0,0
uChar progressuChar;
uChar dialingChannel[HOP_NUM];
uChar usingChannel[HOP_NUM * 4]; // hop not changed
uChar answerChannel[HOP_NUM];
uChar specialf; // special function after reset
uChar volRing;
uChar volHS;
uChar volHF;

uChar sys_state; // sys_state following talkvol

uChar sys_stage;

uChar key_state;
uChar key_code;
uChar key_code_pre;
uChar key_code2;
uChar timer_ledl; // back light led timeout

uChar timer05s;
uChar timer1m;
uChar timersleep;
uChar timer1; // timer 1 count down per 0.5 seconds normally
uChar key_timer0;
uChar key_timer1;
uChar timer_wait_call;
uChar ringcount;
uChar voiceo_ptr;
uChar voice_ptru;
// frame
uChar voicei_ptr;
//uChar count15minl;
//uChar count15minh;
uChar mld_timer;
uChar mld_timer_div;
//USHORT chksum;

uChar handFree;
uChar headNow; // hangup need change,

uChar txNumber;

uChar outid[5] = {0x55, 0x55, 0x55, 0x66, 0x33};
uChar channelGroup;
uChar channelIndex;
uChar rf_buf[PAYLOADLEN];
//uChar sync_count;
uChar need_key;
uChar rateAdj = 0;
uChar lead;
// jmp_buf restartLabel; // for long jmp
//#ifdef SUPRESSTDD

uChar micSoftGainNow;
uChar middleCount;
uChar hfTraining; // when training gain must be small

uChar targetFGP;
// we don't remove tdd for dmi
#if !DMI
USHORT tdd[VOICE_PTR_INC];
#endif
//#endif
/*
__xdata USHORT rssi_val[50];
__xdata uChar rssi_ch[50];
*/

#define TALKVOLMAX 9
#define HSC(x) (2 * x + 1)

__code const uChar playVolTabHF[] = {255, 204, 163, 128, 64, 32, 16, 8, 4, 2};
#if HANDSETPA7
__code const uChar playVolTabHS[] = {255, 204, 163, 128, 64, 32, 16, 8, 4, 2}; // same
#define HSPAG 0x00
#else
__code const uChar playVolTabHS[] = {HSC(32), HSC(24), HSC(20), HSC(16),
                                     HSC(8), HSC(4), HSC(3), HSC(2), HSC(1), HSC(0)};
#define HSPAG 0x01
#endif
const Uint8 __code ID_Tab[8] = {0x34, 0x75, 0xC5, 0x2A, 0xC7, 0x33, 0x45, 0xEA}; //ID code

__code const uChar syncaddr[5] = {0x75, 0x12, 55, 66, 33};

// 2017 change GIO1 to WTR, GIO2 to MISO
__code const uChar A7196Config[] =
    {
        0x00, //RESET register,			only reset, not use on config
        0x62, //MODE register, // directmode pin dis, autoif enable, cd ena, bit 1 fifo mode, disable adc measure
        0x00, //CALIBRATION register,	only read, not use on config
        0x3F, //FIFO1 register,
        0x00, //FIFO2 register,
        0x00, //FIFO register,			for fifo read/write
        0x00, //IDDATA register,0x6		for idcode
        0x00, //RCOSC1 register,0x7
        0x00, //RCOSC2 register,0x8
        0x0C, //RCOSC3 register,0x9
        0xea, //0xa, CKO register, 1 0101 010, output 16MHz 10100 010 , formerly ea out syck
#if GIO1INV
        0x03, // 0xb,GPIO1 register. // 01->WTR, 19->MISO, bit1 is reverse
#else
        0x01, //0xb,GPIO1 register. // 01->WTR, 19->MISO
#endif
        0x19, //0xc, GPIO2 register, // 01->WTR, 19->MISO
        0x2F, //0xd,DATARATECLOCK register, // use external clock set 5e, self xtl set 5f
        0x64, //0xe, PLL1 register,
        0x16, //0xf, PLL2 register, 				RFbase 2400.001MHz
        0x64, //0x10, PLL3 register, // 0x10
        0x00, //0x11, PLL4 register,
        0x02, //0x12, PLL5 register,
        0x3C, //0x13, ChannelGroup1 register,
        0x78, //0x14, ChannelGroup1 register,
        0x2E, //0x15, TX1 register, 				gaussian 0.5T
        0x2B, //0x16, TX2 register,
        0x18, //0x17, DELAY1 register,
        0x40, //0x18, DELAY2 register,
        0x70, //0x19, RX register,
        0xFC, //0x1a, RXGAIN1 register,
        0xCA, //0x1b, RXGAIN2 register,
        0x9C, //0x1c, RXGAIN3 register,
        0xCA, //0x1d, RXGAIN4 register,
        0x00, //0x1e, RSSI register,
        0xF1, //0x1f, ADC register,
#if CRC_EN
        // 0x20h bit 1,0 is ext preamble length, a7196 recommend 00
        // bit 5 is FEC, set 1 will use 7,4 hamming code,
        // bit 6 is WHTS, set 1 will white by PN7
        // bit 7 is multi-crc, used for large payload
        // bit 10 is ext peamble length, 0,1,2,4,bytes
        // formerly 5c, try 7c
        0x7C, // 0x20 bit 4 is crc en, bit 3,2 is id length 11 means 8 bytes

        // 0x21 bit 7: maskcrc
        //MSCRC: Mask CRC (CRC Data Filtering Enable). [0]: Disable. [1]: Enable.
        // bit 6 EDRL
        //EDRL: Enable FIFO Dynamic Length [0]: Disable. [1]: Enable.
        // bit 5: HECS
        //HECS: Head CRC Select [0]: disable. [1]: enable
        // bit 4..2
        //ETH [2:0]: Received SID2 Code Error Tolerance. SID2 is only valid if ID length is 8bytes. Recommend ETH = [011].
        //[000]: 0 bit, [001]: 1 bit. [010]: 2 bit. [011]: 3 bit. [100]: 4 bit, [101]: 5 bit. [110]: 6 bit. [111]: 7 bit.
        // bit 1..0 is PTH
        //PTH [1:0]: Received SID1 Code Error Tolerance. Recommend PTH = [10]. [00]: 0 bit, [01]: 1 bit. [10]: 2 bit. [11]: 3 bit.

        //MTCRCF [7:0]: Sub-package CRC Flag (read only). Please refer to section 16.1.3
        0x0E,
#else
        0x0C, //0x20, CODE1 register,// default 07? preamble 4 uChars, id 4 uChars, 0x20!! bit6 is whit bit 4 is crc
        0x0E, //0x21, CODE2 register,// default 05? EDRL = 0 , CRC enable=bit 7
#endif
        0x00,          //0x22, CODE3 register,// use 0 for 000
        0x05,          //0x23, IFCAL1 register,
        0x01,          //0x24, IFCAL2 register,    0x7F
        0xCF,          //0x25, VCOCCAL register,
        0xD0,          //0x26, VCOCAL1 register,
        0x80,          //0x27, VCOCAL2 register,
        0x30,          //0x28, VCO deviation 1 register,
        0x36,          //0x29, VCO deviation 2 register,	//28
        0x00,          //0x2a, DSA register,
        0xD0,          //0x2b, VCO Modulation delay register,	//9C
        0x70,          //0x2c, BATTERY register,
        0x7F,          //0x2d, TXTEST register,
        0x57,          //0x2e, RXDEM1 register,5F
        0x74,          //0x2f, RXDEM2 register,
        0xF3,          //0x30, CPC1 register, // charge pump1, address 0x30
        0x33,          //0x31, CPC2 register,
        0x4D,          //0x32, CRYSTAL register,0x51
        0x19,          //0x33, PLLTEST register,
        0x0A,          //0x34, VCOTEST register,
        RFANA_DEFAULT, //0x35, RF Analog register, address 0x35
        0x00,          //0x36, Key data register,
        0x37,          //0x37, Channel select register,
        0x00,          //0x38, ROM register,
#if MBPS1
        0x01,
#else         
        0x00,          //0x39, DataRate register,
#endif        
        0x00,          //0x3a, FCR register, EAK/EAR=0, no auto ack/resend
        0x00,          //0x3b, ARD register,
        0x00,          //0x3c, AFEP register, EAF=0
        0x00,          //0x3d, FCB register,
        REG3E_DEFAULT, //KEYC register,
        0x00           //USID register,
};

__code const Uint8 A7196_Addr2A_Config[] =
    {
        0x39, //page0,
        0x09, //page1,
        0xF0, //Page2,
        0x80, //page3,
        0x80, //page4,
        0x08, //page5,
        0x82, //page6,
        0xC0, //page7,
        0x00, //page8,
        0x3C, //page9,
        0xE8, //pageA,
        0x00, //pageB,
};

__code const Uint8 A7196_Addr38_Config[] =
    {
        0x4D, //page0,
        0x00, //page1,
        0x30, //page2,
        0xB4, //page3,
        0x20, //page4,
        0x90, //page5,
        0x00, //page6,
        0x00, //page7,
        0x00, //page8,
};

// set 0 to key first?
__code const Uint8 KeyData_Tab[16] = {0x00, 0x00,
                                      0x00, 0x00, 0x00, 0x0, 0x00, 0x00, 0x00, 0x00,
                                      0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

#define ram_mark syncaddr

volatile uChar timer0;
void key_machine(void);
uChar keyTemp;
void play_to_finish(void);
void timer_routine(void) __interrupt
{
    if (!TOV)
        return;

    TOV = 0;
    need_key = 1;
    if (timer0)
        timer0--;
    if (timer_wait_call)
        --timer_wait_call;
    //	if(beep_timer)
    //		--beep_timer;
    if (++mld_timer_div == 16) // 32 Hz
    {
        if (mld_timer)
            mld_timer--;
        mld_timer_div = 0;
    }
    if (++timer05s == 250)
    {
        timer05s = 0;
        if (timer1)
            timer1--;

        if (timer_ledl)
        {
            timer_ledl--;
            if (!timer_ledl)
                PIOA |= PA_LEDL;
        }
        if (timer1m)
        {
            timer1m--;
        }
        else
        {
            timer1m = 120;
            if (timersleep)
                --timersleep;
        }
    }
}

unsigned short getRSSI(void)
{
    uChar tmp;

    SPI_Write_Strobe(CMD_RX); // by ds, stay in RX for 140us,
                              // the rssi should be ready
    delay2nms(1);
    tmp = SPI_Read_Reg(RSSI_REG);

    SPI_Write_Strobe(CMD_STBY);
    return (unsigned short)(tmp * tmp);
}
uChar selectDialOutChannel(void)
{
    // use RSSI to get dial out channel cleaness
    uChar i, j = 0;
    unsigned long min = 0x7fffffff;
    unsigned long rssi[4]; // check 4 groups
    rssi[0] = 0;
    rssi[1] = 0;
    rssi[2] = 0;
    rssi[3] = 0;
    SPI_Write_Strobe(CMD_STBY);
#if SIMULATION
    for (i = 0; i < 2; i++)
#else
    for (i = 0; i < 50; i++)
#endif
    {
        SPI_Write_Reg(PLL1_REG, usingChannel[0])
            rssi[0] += getRSSI();
        SPI_Write_Reg(PLL1_REG, usingChannel[1])
            rssi[0] += getRSSI();
        SPI_Write_Reg(PLL1_REG, usingChannel[2])
            rssi[1] += getRSSI();
        SPI_Write_Reg(PLL1_REG, usingChannel[3])
            rssi[1] += getRSSI();
        SPI_Write_Reg(PLL1_REG, usingChannel[4])
            rssi[2] += getRSSI();
        SPI_Write_Reg(PLL1_REG, usingChannel[5])
            rssi[2] += getRSSI();
        SPI_Write_Reg(PLL1_REG, usingChannel[6])
            rssi[3] += getRSSI();
        SPI_Write_Reg(PLL1_REG, usingChannel[7])
            rssi[3] += getRSSI();
    }
    // now sort them
    for (i = 0; i < 4; i++)
    {
        if (rssi[i] < min)
        {
            j = i;
            min = rssi[i];
        }
    }
    return j;
}
void key_machine(void)
{
    if (key_state == KEYS_NOKEY)
    {
        if (!key_code)
        {
            key_code_pre = get_key();
            if (key_code_pre)
            {
                key_timer0 = KEY_DEB_TIME; // debounce
                key_state = KEYS_DEB;
            }
        }
        return;
    }
    else if (key_state == KEYS_DEB)
    {
        if (!--key_timer0)
        {
            keyTemp = get_key();
            if (keyTemp == key_code_pre || (keyTemp == KEY_CODE_SYNC &&
                                            (key_code_pre == KEY_CODE_TALK || key_code_pre == KEY_CODE_HANGUP)))
            {

                if (key_code_pre == KEY_CODE_ONOFF)
                {
                    key_state = KEYS_WAIT_OFF;
                    key_timer0 = 250;
                    key_timer1 = 6; // 3 seconds
                }
                else if ((key_code_pre == KEY_CODE_HANGUP ||
                          key_code_pre == KEY_CODE_TALK) &&
                         sys_state == SYS_IDLE)
                {
                    key_state = KEYS_WAIT_SYNC;
                    key_timer0 = 250;
                    key_timer1 = 2;
                }
                else
                {
                preandrelease:
                    key_code = key_code_pre;
                    goto towrelease;
                }
            }
            else
            towrelease:
                key_state = KEYS_WAIT_RELEASE;
        }
    }
    else if (key_state == KEYS_WAIT_SYNC)
    {
        key_code2 = get_key();
        if (get_key() == KEY_CODE_TEST)
        {
            key_code = KEY_CODE_TEST;
            goto towrelease;
        }
        if (!key_code2)
        {
            key_code = key_code_pre;
            //key_state=KEYS_WAIT_RELEASE;
            goto towrelease;
        }
        else
        {
            if (key_code2 == KEY_CODE_SYNC)
            {
                key_state = KEYS_SYNCK_DELAY;
                key_timer0 = 0;
                key_timer1 = 0;
            }
            else
            {
                if (!--key_timer0)
                {
                    if (!--key_timer1)
                    {
                        //key_code=key_code_pre;
                        //key_state=KEYS_WAIT_RELEASE;
                        goto preandrelease;
                    }
                }
            }
        }
    }
    else if (key_state == KEYS_SYNCK_DELAY)
    {
        if (!get_key())
        {
            //key_state=KEYS_WAIT_RELEASE;
            //break; // give up
            goto towrelease;
        }
        if (!++key_timer0)
        {
            if (++key_timer1 == 5)
            {
                key_code = KEY_CODE_SYNC;
                //key_state=KEYS_WAIT_RELEASE;
                goto towrelease;
            }
        }
    }
    else if (key_state == KEYS_WAIT_RELEASE)
    {
        if (!get_key())
        {
            key_state = KEYS_NOKEY;
        }
    }
    else if (key_state == KEYS_WAIT_OFF)
    {

        if (get_key() != KEY_CODE_ONOFF)
        {
#if SIMULATION
            if (key_timer0 < 250 || key_timer1 != 6)
#else
            if (key_timer0 < 240 || key_timer1 != 6)
#endif
            {
                key_code = KEY_CODE_HF; // hand free
            }
            goto towrelease;
        }
        if (!--key_timer0)
        {
            if (!--key_timer1)
            {
                key_code = KEY_CODE_ONOFF;
                goto towrelease;
            }
            key_timer0 = 250;
        }
    }
}

void ioint_routine(void) __interrupt
{
}
void dmaint_routine(void) __interrupt
{
}
void wdt_routine(void) __interrupt
{
    GIF = 0xfd; // clear it
}

void beep2ifNotIdle(void)
{
    if (!sys_state)
        return;
    delay2nms(KEY_BEEP_LEN);
    beep_short(KEY_BEEP_LEN * 2);
    delay2nms(KEY_BEEP_LEN);
    beep_short(KEY_BEEP_LEN * 2);
}
uChar SPI_Read_Reg(uChar addr)
{
    uChar read_data;
    cslow();
    SPIDAT = addr | 0x40; // a7237 use bit6 as r/wb
    read_data = SPIDAT;
    cshigh();
    return read_data;
}
void SPI_Read_Buf_PL(uChar addr) // input is ROMP
{
    uChar i = 0;
    // for ms326 rom size is not a issule, but
    // speed is, so we split them
    // input is ROMPLH
    cslow();
    SPIDAT = addr | 0x40;
    SDMAALH = (USHORT)(&rf_buf[0]);
    SDMALEN = PAYLOADLEN - 1;
    SPIDMAC = 0x06;
    while (SPIDMAC & 0x80)
        ;
    cshigh();
    GIF = 0xDF; // clear the flag
}

#if (!SPIMACRO)
void SPI_Write_Reg(uChar reg, uChar value)
{
    cslow();
    SPIDAT = reg;
    SPIDAT = value;
    cshigh();
}
void SPI_Write_Strobe(uChar cmd)
{
    cslow();
    SPIDAT = cmd;
    cshigh();
}
#endif
void SPI_Write_Reg_Page(uChar reg, uChar value, uChar page)
{
    SPI_Write_Reg(RFANALOG_REG, (RFANA_DEFAULT & 0x0f) | (page << 4));
    SPI_Write_Reg(reg, value);
}
void SPI_Write_Buf_RAM(uChar add) // to rf spi!!
{
    cslow();
    // SPLIT for speed!!
    SPIDAT = add;

    SDMAAL = ROMPL;
    SDMAAH = ROMPH;

    SDMALEN = PAYLOADLEN - 1;
    SPIDMAC = 2;
    while (SPIDMAC & 0x80)
        ;

    cshigh();
    GIF = 0xDF; // clear the flag
}

uChar read_packet(uChar purge)
{
    if (RFINTPIN)
    {
        if (purge)
            SPI_Write_Strobe(CMD_RFR);
        return 0xff;
    }
    ROMPLH = &rf_buf[0];
    SPI_Read_Buf_PL(FIFO_REG); // FULL PAYLOAD
    if (purge)
        SPI_Write_Strobe(CMD_RFR); // rx fifo reset

    return 0;
}
unsigned char lowCount;
unsigned char highCount;
// in qechk.s
/*
void adjLevel(unsigned char tag)
{
    if (handFree)
    {
        // FILTERGP
        if (tag == 0)
        {
            ++lowCount;
            if (lowCount > 20)
            {
                if (FILTERGP > INI_FGP)
                    FILTERGP--;
                lowCount = 0;
            }
            highCount = 0;
        }
        else
        {
            ++highCount;
            if (highCount > 10)
            {
                if (FILTERGP < targetFGP)
                {
                    unsigned char k = FILTERGP + 10; // loud faster
                    if (k < targetFGP)
                        FILTERGP = k;
                    else
                    {
                        FILTERGP = targetFGP;
                    }
                }
                highCount = 0;
            }
            lowCount = 0;
        }

        if (tag == 3)
        {
#if DMI
            micSoftGainNow = 40; // small gain, train slowly
#else
            micSoftGainNow = 10; // small gain, train slowly
#endif
        }
        else if (tag >= 2) // if other side loud, we quiet
        {
#if DMI
            if (micSoftGainNow > 30) // to very small
                micSoftGainNow = (micSoftGainNow * 250) >> 8;
#else
            if (micSoftGainNow > 20)
                micSoftGainNow -= 5;
#endif
        }
        else if (tag == 0)
        {
#if DMI
            if (micSoftGainNow < DMIGAINHF)
                micSoftGainNow++;
#else
            if (micSoftGainNow < MICGAIN_ANA)
                micSoftGainNow++;
#endif
        }
        else // depends on middle count
        {
            if (++middleCount >= 20)
            {
#if DMI
                if (micSoftGainNow < DMIGAINHF)
                    micSoftGainNow++;
#else
                if (micSoftGainNow < MICGAIN_ANA)
                    micSoftGainNow++;
#endif
            }
        }
    }
}
*/
uChar temp000;
uChar temp001;
extern void adjLevel(unsigned char);
uChar read_voice_packet(void)
{
    uChar tag;
    uChar len1;

    // voice packet first uChar MSB=0
    if (!(RFINTPIN))
    {
        timer1 = 2;
        cslow();
        SPIDAT = FIFO_REG | 0x40; // read
        tag = SPIDAT;             // TAG
        if ((tag & TAG_VOICE_MASK) != TAG_VOICE_CHK)
        {
            SPI_Write_Strobe(CMD_RFR);
            return tag;
        }
        // fast quiet slow up
        tag &= TAG_VOICE_LOUDNESS;
        adjLevel(tag);

        voicei_ptr = SPIDAT; // THIS IS FIRST uChar
        SDMAAL = voicei_ptr;
        SDMAAH = PLAY_BUFAH;

        if (voicei_ptr < (257 - VOICE_LOAD_LEN))
        {
            // one time
            SDMALEN = VOICE_LOAD_LEN - 1;
            SPIDMAC = 0x06;
            while (SPIDMAC & 0x80)
                ;
        }
        else
        {
            len1 = 256 - voicei_ptr;
            SDMALEN = len1 - 1;
            SPIDMAC = 0x06;
            while (SPIDMAC & 0x80)
                ;
            SDMAAL = 0;
            SDMAAH = PLAY_BUFAH;
            SDMALEN = VOICE_LOAD_LEN - len1 - 1;
            SPIDMAC = 0x06;
            while (SPIDMAC & 0x80)
                ;
        }

        cshigh();
        GIF = 0xDF; // clear the flag
        return tag;
    }

    SPI_Write_Strobe(CMD_RFR);
    return 0;
}
void enter_ringing_mode(void)
{
    // if 2 second no signals, we stop the ring
    sw_to_osc();
    timer1 = 9; // change to 9
    sys_state = SYS_RING;
    api_set_vol(0x3f, playVolTabHF[volRing]);
    API_PSTARTH(incoming);
}
void next_channel_ce0(unsigned char talking)
{
#if HOPINDEX_MAX > 1
    uChar i;
#endif
#if !SINGLECHAN
    channelIndex = (channelIndex + 1) & HOPINDEX_MAX; //debug no sw channel
#endif

    // if (talking == 2)
    //     ROMPLH = &answerChannel[channelIndex];
    // else if (talking)
    //     ROMPLH = &usingChannel[(channelGroup << 1) + channelIndex];
    // else
    //     ROMPLH = &dialingChannel[channelIndex];

    if (talking == 2)
    {
        ROMPLH = &answerChannel[0];
    }
    else if (talking)
    {
        unsigned char s = channelGroup << 1;
        ROMPLH = &usingChannel[s];
    }
    else
    {
        ROMPLH = &dialingChannel[0];
    }
    // all data in zero page
#if HOPINDEX_MAX > 1
    i = channelIndex;

    while (i)
    {
        __asm LDA @_ROMPINC __endasm;
        --i;
    }
#else
    if (channelIndex)
    {
        __asm LDA @_ROMPINC __endasm;
    }
#endif

    SPI_Write_Strobe(CMD_STBY);
    SPI_Write_Reg(PLL1_REG, ROMP);
    SPI_Write_Strobe(CMD_TFR);
    SPI_Write_Strobe(CMD_RFR);
    SPI_Write_Strobe(CMD_RX);
}
//---------------------------------------------------------------------------
uChar mappedid[4];

void A7196_KeyData(void)
{
    uChar i;
    cslow();
    SPIDAT = KEYDATA_REG;
    ROMPLH = (uChar *)&KeyData_Tab[0];
    for (i = 0; i < 16; i++)
        SPIDAT = ROMPINC;
    cshigh();
}

void set_rf_id(void)
{

    uChar i;
    cslow();
    SPIDAT = IDCODE_REG;
    for (i = 0; i < 8; i++)
        SPIDAT = ID_Tab[i];
    cshigh();
}
void rf_reset(void)
{
    SPI_Write_Reg(MODE_REG, 0);
}
const uChar __code FCB_Tab[20] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
void A7196_FCB(void)
{
    uChar i;
    cslow();
    SPIDAT = FCB_REG;
    for (i = 0; i < 20; i++)
        SPIDAT = FCB_Tab[i];
    cshigh();
}
void rf_config(void)
{
    uChar i;
    ROMPLH = (uChar *)&A7196Config[1];
    for (i = 0x1; i <= 2; i++)
        SPI_Write_Reg(i, ROMPINC);
    cslow();
    SPIDAT = 0x03;
    SPIDAT = 0x3F;
    SPIDAT = 0x00;
    cshigh();

    SPI_Write_Reg(0x04, A7196Config[0x04]);

    ROMPLH = (uChar *)&A7196Config[7];
    for (i = 0x07; i != 0x2a; i++)
        SPI_Write_Reg(i, ROMPINC);

    ROMPLH = (uChar *)&A7196_Addr2A_Config[0];
    for (i = 0; i != 12; i++) //0x2A DAS
        SPI_Write_Reg_Page(0x2A, ROMPINC, i);

    ROMPLH = (uChar *)&A7196Config[0x2b];
    for (i = 0x2B; i != 0x36; i++)
        SPI_Write_Reg(i, ROMPINC);

    A7196_KeyData();

    SPI_Write_Reg(0x37, REG37_DEFAULT);

    ROMPLH = (uChar *)&A7196_Addr38_Config[0];
    for (i = 0; i != 9; i++) //0x38 ROM
        SPI_Write_Reg_Page(0x38, ROMPINC, i);

    ROMPLH = (uChar *)&A7196Config[0x39];

    for (i = 0x39; i != 0x3d; i++)
        SPI_Write_Reg(i, ROMPINC);

    A7196_FCB();
    SPI_Write_Reg(0x3E, REG3E_DEFAULT);
    SPI_Write_Reg(0x3F, A7196Config[0x3f]);
}
void CHGroupCal(Uint8 ch)
{
    Uint8 tmp;
    Uint8 vb, vbcf, vcb, vccf, adag;
    Uint8 deva, adev;

    SPI_Write_Reg(PLL1_REG, ch);
    SPI_Write_Reg(CALIBRATION_REG, 0x1C);
    do
    {
        tmp = SPI_Read_Reg(CALIBRATION_REG) & 0x1C;
    } while (tmp);

    //for check
    tmp = SPI_Read_Reg(VCOCCAL_REG);
    vcb = tmp & 0x0F;
    vccf = (tmp >> 4) & 0x01;

    tmp = SPI_Read_Reg(VCOCAL1_REG);
    vb = tmp & 0x07;
    vbcf = (tmp >> 3) & 0x01;

    tmp = SPI_Read_Reg(VCOCAL2_REG);
    adag = tmp;

    tmp = SPI_Read_Reg(VCODEVCAL1_REG);
    deva = tmp;

    tmp = SPI_Read_Reg(VCODEVCAL2_REG);
    adev = tmp;

    if (vbcf || vccf)
        warmRst(); //error
}

void rf_cal(void)
{
    Uint8 tmp;
    Uint8 fb, rhc, rlc;
    Uint8 fbcf, fcd;

    SPI_Write_Strobe(CMD_PLL); //calibration @PLL state
    SPI_Write_Reg(CALIBRATION_REG, 0x23);
    do
    {
        tmp = SPI_Read_Reg(CALIBRATION_REG) & 0x23;
    } while (tmp);
    //calibration VBC,VDC procedure
    CHGroupCal(30);             //calibrate channel group Bank I
    CHGroupCal(90);             //calibrate channel group Bank II
    CHGroupCal(150);            //calibrate channel group Bank III
    SPI_Write_Strobe(CMD_STBY); //return to STBY state

    tmp = SPI_Read_Reg(IFCAL1_REG);
    fb = tmp & 0x0F;
    fbcf = (tmp >> 4) & 0x01;

    fcd = SPI_Read_Reg(IFCAL2_REG) & 0x1F;

    rhc = SPI_Read_Reg(RXGAIN2_REG);
    rlc = SPI_Read_Reg(RXGAIN3_REG);

    //tmp = SPI_Read_Reg_Page(0x2A, 8);
    //    rcrs = tmp & 0x0F;
    //    tmp1 = (tmp & 0xF0) >> 4;

    //tmp = SPI_Read_Reg_Page(0x2A, 9);
    //    rcts = (tmp & 0x03) << 4 | tmp1;
    //    tmp1 = (tmp & 0xF0) >> 4;

    //tmp = SPI_Read_Reg_Page(0x2A, 10);
    //    rct = tmp | ((tmp1)*256);

    if (fbcf)
        warmRst();

    // //calibration IF procedure
    // loopCnt = 0;

    // while (1)
    // {
    //     SPI_Write_Reg(CALIBRATION_REG, 0x02);
    //     do
    //     {
    //         tmp = SPI_Read_Reg(CALIBRATION_REG);
    //         tmp &= 0x02;
    //     } while (tmp);

    //     tmp = SPI_Read_Reg(IFCAL1_REG);
    //     fb = tmp & 0x0F;
    //     if (fb > 3 && fb < 10)
    //         break;

    //     loopCnt++;
    //     if (loopCnt == 10)
    //         warmRst();
    // }

    // //if (loopCnt ==10)
    // //warmRst(); // this will reset

    // //calibration RSSI, VCC procedure
    // SPI_Write_Reg(CALIBRATION_REG, 0x11);
    // do
    // {
    //     tmp = SPI_Read_Reg(CALIBRATION_REG);
    //     tmp &= 0x11;
    // } while (tmp);

    // //calibration VBC,VDC procedure
    // CHGroupCal(30);             //calibrate channel group Bank I
    // CHGroupCal(90);             //calibrate channel group Bank II
    // CHGroupCal(150);            //calibrate channel group Bank III
    // SPI_Write_Strobe(CMD_STBY); //return to STBY state

    // //for check

    // tmp = SPI_Read_Reg(IFCAL1_REG);
    // fb = tmp & 0x0F;
    // fbcf = (tmp >> 4) & 0x01;

    // tmp = SPI_Read_Reg(IFCAL2_REG);
    // fcd = tmp & 0x1F;

    // tmp = SPI_Read_Reg(VCOCCAL_REG) & 0x1F;
    // vcb = tmp & 0x0F;
    // vccf = (tmp >> 4) & 0x01;

    // rhc = SPI_Read_Reg(RXGAIN2_REG);
    // rlc = SPI_Read_Reg(RXGAIN3_REG);

    // if (fbcf || vccf)
    //     warmRst();
}

void gen_default_chan(void)
{

    // we steal 2475~2482
    uChar i;
    unsigned short j;
    j = DEFAULT_CHAN_ST;
    ROMPLH = &(dialingChannel[0]);
    for (i = 0; i < HOP_NUM * 5; i++)
    {
        ROMPINC = (uChar)j;
        j = j + DEFAULT_CHAN_INC;
        if (j >= 149)
            j -= 149;
    }
}
void delay2nms(uChar c)
{

    timer0 = c;
    while (timer0)
    {
        api_enter_stdby_mode(0, 0, 0, 0, 0);
        if (GIF & 0xc0)
            GIF = 0x3f; // clean dma wakeup flag
    }
}

void init(void)
{

#if ICE_DBG
    SYSC = SYSC_BIT_LPASS;
#else
    SYSC = SYSC_BIT_SKCMD | SYSC_BIT_LPASS; // LPASS!!
#endif
    gen_default_chan();
    API_USE_IRC;

    PBR = 0x5f; // PIOB5 no PH, but PIOB6 give PH, PB7 not, because CKO is out
    WDTL = 0;   // disable wdt, anyway

    PIOC = 1;     // CS HIGH
    PCDIR = 0x07; // RFSPI, A7196 MAX is 10MHZ, /4 is fine
    RSPIC = 0xe0; // use 2MHZ, slow but should be enough
    // PIOA0,1,2: KEY scan source .. new pcb may change
    // PIOA3,4,5: LED
    // PIOA6: alt. rf interrupt
    // PIOA7 is PWM output, default is 0
    PIOA = PA_LED_ALL;
    PADIR = PADIR_DEFAULT;

    PAR = 0xff;
    PBDIR = 0x00; //PIOB6 give high!!
    PIOB = 0x00;

    // PIOB0..4 is key scan input
    // PIOB5 should be GIO1
    // PB6,7 may be used as crystal in
    // PIOB7 is clock in

    //count15minh = 15; // 15 minutes around

    ROMPH = REC_BUFAHDBG; // 5xx for debug
    ROMPL = 0;
    do
    {
        ROMPINC = ROMPL;
    } while (ROMPL);
    timersleep = 15; // 15 minute
    timer_wait_call = WAIT_CALL_TIME;
    // address at 0x3FFC
    // this is generally no use
    ROMPLH = (uChar *)0x3FF7;
    RX0_Address[1] = ROMPINC; //3ff7
    RX0_Address[2] = ROMPINC;
    RX0_Address[3] = ROMPINC ^ 0xAA;
    RX0_Address[4] = ROMPINC ^ 0x55;
    if (RX0_Address[1] == 0x11 && RX0_Address[2] == 0x33)
    {
        // keep even not used
        volRing = ROMPINC ^ 0xff; // 3ffb
        volHS = ROMPINC ^ 0xff;
        volHF = ROMPINC ^ 0xFF;
        myId = ROMPINC ^ 0xff; // at 3ffe
    }
    else
    {
        myId = 0;
        volRing = 3;
        volHS = HANDSETVOL_DEFAULT;
        volHF = 3;
    }
    RX0_Address[1] = 0;
    RX0_Address[2] = 0;

    if (volRing > TALKVOLMAX)
        volRing = 3;
    if (volHS > TALKVOLMAX)
        volHS = HANDSETVOL_DEFAULT;
    if (volHF > TALKVOLMAX)
        volHF = 3;

    INTVC = 0;
    INTV = (uChar)(&timer_routine);
    INTVC = 1;
    INTV = ((USHORT)timer_routine) >> 8;
    INTVC = 2;
    INTV = (uChar)(&dmaint_routine);
    INTVC = 3;
    INTV = ((USHORT)dmaint_routine) >> 8;
    INTVC = 4;
    INTV = (uChar)(&wdt_routine);
    INTVC = 5;
    INTV = ((USHORT)wdt_routine) >> 8;
    INTVC = 6;
    INTV = (uChar)(&ioint_routine);
    INTVC = 7;
    INTV = ((USHORT)ioint_routine) >> 8;
    api_timer_on(TMAH_RELOAD);
    GIE = 1; //timer use interrupt routine

    EA = 1;
}
#if PAKEY345
#define KEYOH PIOA |= 0x38
#define KEYOL PIOA &= 0xc7
#else
#define KEYOH PIOA |= 0x07
#define KEYOL PIOA &= 0xF8
#endif
// bit 0: return 0
// bit 1: return 3
// bit 2: return 6 ...

const uChar pb_code[32] = {
    0,
    0,
    3,
    0,
    6, // ind=4
    0,
    3,
    0,
    9, // 8
    0,
    3,
    0,
    6, //12
    0,
    3,
    0,
    12, // 16
    0,
    3,
    0,
    6, // 20
    0,
    3,
    0,
    9, // 24
    0,
    3,
    0,
    6, // 28
    0,
    3,
    0};
// get_key
// assume key is from PA0,1,2 to PB0..4

#ifndef __SDCC
uChar get_key(void)
#else
uChar get_key(void) __critical
#endif
{
    uChar rb0, rb1, rb2;
    // new circuit pa012 => pb0~4

    if ((PIOB & 0x1f) == 0x1f)
        return 0;
    KEYOH;

    PADIR = PA_KEYS_INV | PA_KEYS_R1 | PA_PA7PWM;
    KEYOL;
    rb0 = (PIOB ^ 0xff) & 0x1f;
    KEYOH;
    PADIR = PA_KEYS_INV | PA_KEYS_R2 | PA_PA7PWM;
    KEYOL;
    rb1 = (PIOB ^ 0xff) & 0x1f;
    KEYOH;
    PADIR = PA_KEYS_INV | PA_KEYS_R3 | PA_PA7PWM;
    KEYOL;
    rb2 = (PIOB ^ 0xff) & 0x1f;
    PADIR = PADIR_DEFAULT;
    KEYOL;
    if (rb0 == 1)
    {
        if (rb2 == 1)
            return KEY_CODE_SYNC;
        if (rb1 == 0x10)
            return KEY_CODE_TEST;
    }

    if (rb2)
        return pb_code[rb2] + 1;
    if (rb1)
        return pb_code[rb1] + 2;
    if (rb0)
        return pb_code[rb0] + 3;
    return 0;
}
// main at botton

// 2018 new board change
//
void sleep(uChar singlepinwk) // 0: PB0~4 wakeup , 1: PB0 wakeup
{
    uChar wk = singlepinwk ? 1 : 0x1f; // pb3 or pb1234

    SPI_Write_Strobe(CMD_SLEEP);

    // we need to check ce high/low power consumption , stdby1 is 50ua, fine.
    if (!singlepinwk)
    {

        PIOA = PA_LED_ALL;
        PADIR |= PA_KEYS | PA_PA7PWM;
    }
    else
    {

        PIOA = PA_LED_ALL;
        PADIR = PA_LED_ALL | PA_ONOFF_ROW | PA_PA7PWM; // ONOFF use PIOA1
    }
    api_normal_sleep(0, 0, wk, 0, 1, 0);
    WDTL = 4;

    // following instruction cannot be PA, because sometimes it runs one more
    timer_wait_call = WAIT_CALL_TIME;
    TIMERC = 3; // reset it
#if !ICE_DBG
    SYSC |= SYSC_BIT_SKCMD; // faster
#endif
    SPI_Write_Strobe(CMD_STBY);
    flushtr();

    SPI_Write_Strobe(CMD_RX); //
}
void enter_idle_mode(void)
{
    PIOA |= PA_LEDR; // LEDR OFF!!
    PIOA &= 0x7f;    // PIOA7 must be low
    //count15minh = 0; // reset 15 min counter
    DACON = 0;
    ADCON = 0; // turn off them first!!
    DMICON = 0;
    if (sys_state == SYS_IDLE)
        return;

    //if (sys_state == SYS_SYNC && sys_stage >= 3)
    {
        sys_state = SYS_IDLE;
        //set_new_addr(); // no need because it will reset
        SPI_Write_Reg(CODE3_REG, myId);
        SYSC &= ~SYSC_BIT_SKCMD;
        API_SPI_READ_PAGE(0x3f, 0x4); // 0x3fxx to 0x400~0x4ff
        API_SPI_ERASE(0x30);
        ROMPLH = (uChar *)0x84f7; // 3ffe is the id, use ram 400~4FF
        ROMPINC = 0x11;
        ROMPINC = 0x33;
        ROMPINC = 0x55;
        ROMPINC = 0xaa;
        ROMPINC = volRing ^ 0xff;
        ROMPINC = volHS ^ 0xff;
        ROMPINC = volHF ^ 0xff;
        ROMPINC = myId ^ 0xff;
        API_SPI_WRITE_PAGE(0x3f, 0x4); // write ram

        //backup_ram();
    }

    warmRst(); // just reset because MS322B has bugs
}
//uChar ec_state;
uChar keyud_chk(void) // mode0 is handset, 1 is handfree
{
    if (key_state == 0 && (PIOB & 0x1f) == 0x1f)
        return 0;
    key_machine();
    if (key_code == 0)
        return 0;

    if (key_code == KEY_CODE_HANGUP)
    {
        key_code = 0;
        return 1;
    }
    if (key_code == KEY_CODE_UP)
    {
        vol_up();
    }
    else if (key_code == KEY_CODE_DN)
    {
        vol_dn();
    }
    else if (key_code == KEY_CODE_HF) // toggle hand free
    {
        handFree ^= 1;
    changevol:
        if (handFree)
        {
            ADCG = OPAG_HF;
            ECCON = ECCON_DEFAULT;
            hfTraining = 20;
            api_set_vol(0x3f, playVolTabHF[volHF]);
            targetFGP = playVolTabHF[volHF];
            FILTERGP = 0; // re sync again
#if HANDSETPA7
            SPKC &= (0x48 ^ 0xff);
            DACON |= 1;
#endif
#if DMI
            micSoftGainNow = 20;
#endif
        }
        else
        {
            //ec_state=0;
            headNow = TAG_VOICE_CHK + 1;
            ADCG = OPAG_HS;
            ECCON = 0;
            hfTraining = 0;
            api_set_vol(HSPAG, playVolTabHS[volHS]);
#if HANDSETPA7
            SPKC |= 0x68;  // hpf enable!!
            DACON &= 0xfe; // spkoe set 0
#endif
#if DMI
            micSoftGainNow = DMIGAINHS;
#endif
        }
    }

    key_code = 0;
    return 0;
}

/*
void clear_play_buf(void)
{
    ROMPL = 0;
    ROMPH = PLAY_BUFAH;
    do
    {
        ROMPINC = 0; // ff is endcode, which is the very max
    } while (ROMPL);
}
*/

//uChar lastdiff;

// for 7137 clock, sync is not need
// consider 256 circular buffer, per load 62 sample,
// how to make the dac play and rfspi write very far?
// that is , if dac at 0,
// we hope rf is write 0x80-31~0x80+31, tolarence 5

void voice_sync(uChar pktcnt)
{
    // off->on and on to off

    if (FILTERGP == 0) // start play
    {

        if (pktcnt > (768 / VOICE_PTR_INC) && voicei_ptr > (0x81 - VOICE_PTR_INC - VOICE_LOAD_LEN) && voicei_ptr < (0x81 + VOICE_PTR_INC - VOICE_LOAD_LEN))
        {
            uChar fifostart = PLAY_BUFAH << 4;
            uChar fifoend = (PLAY_BUFAH << 4) | 0x0f;

            // set the pointers
            ADP_IND = 0x80;
            PPAGES = (fifostart & 0xf) | ((fifoend & 0xf) << 4);
            ADP_IND = 0;
            PPAGES = (fifostart >> 4) | ((fifoend & 0xf0) >> 1);
            RCLKDIV &= 0x7f; // if use 32MHZ it must | 0x80!!
                             // 7->1ff 1ff = ((7+1)<<6)-1

#if EC_EN

            ECLEN = EC_LEN;
            ECRALH = EC_BUFC;
            ECOALH = EC_BUFO;
            ECMODE = 0;
#endif
            RCLKDIV &= 0x7f; // OSR HIGH
            PDMAH = 0x80;    // reset DMA

            DACON = 0x1A; // reset and start

#if EC_EN

            if (handFree)
            {
                ADCG = OPAG_HF;
                targetFGP = playVolTabHF[volHF]; // quietest first
                FILTERGP = INI_FGP;              // it will increase later
                ECCON = ECCON_DEFAULT;           // always enable
                PAG = 0x3f;
                SPKC &= (0x48 ^ 0xff); // bit 6 set 1 mans pwm mode, 0 is ti mode, bit 3 is pa7o
                DACON |= 1;
#if DMI
                //micSoftGainNow = DMIGAINHF; it will change slowly
#else
                micSoftGainNow = MICGAIN_ANA;
#endif
            }
            else
            {
                ADCG = OPAG_HS;
                FILTERGP = playVolTabHS[volHS]; // quietest first
                ECCON = 0;
                PAG = HSPAG;
                DACON |= 1; // this is still necessary when synced
#if HANDSETPA7
                SPKC |= 0x68; // PA7 EAR OUT, bit 6 0 means TI mode, bit 5 is hpf!!
                DACON &= 0xfe;
                PADIR |= PA_PA7PWM;
#endif
#if DMI
                //micSoftGainNow = DMIGAINHS; // it must be changed gradually
#else
                micSoftGainNow = MICGAIN_ANA;
#endif
            }
#endif
        }
    }

    else
    {
        // when spi dma will write cross the dac dma pointer
        // per cycle 3ms , guard 24 sample
        // dac period default
        uChar chk1 = PDMAL - 30;
        lead = chk1 - voicei_ptr; // globl for simulation

        // the method seems not good for EC
        // but it should be ok for non-EC case

        // if dac period adjust 1/576
        // per second 1.7ms offset
        // that is, 576 sample offset 1 sample, 0.X second will get it back
        if (chk1 < 128)
        {
            if (rateAdj == 0) // check fast or slow
            {
                if (lead < (VOICE_LOAD_LEN + 10)) // need play faster
                {
                    // there is upper/lower problem, but it
                    DAC_PH = (((REC_RATE + 1) << 6) - 2) >> 8; // new rate!!
                    DAC_PL = (((REC_RATE + 1) << 6) - 2) & 0xff;
                    rateAdj = 1; // 1 means tune faster
                    //ECCON &= 0xfd; // ecu off
                    PIOA &= PA_LEDADJ1_INV;
                }
                else if (lead > (VOICE_LOAD_LEN + 60)) // player slower
                {
                    DAC_PH = (((REC_RATE + 1) << 6) - 0) >> 8; // new rate!!
                    DAC_PL = (((REC_RATE + 1) << 6) - 0) & 0xff;
                    rateAdj = 2; // 1 means tune faster
                    //ECCON &= 0xfd; // ecu off
                    PIOA &= PA_LEDADJ2_INV;
                }
            }
            else if (rateAdj == 1) // now tune faster
            {
                if (lead > (VOICE_LOAD_LEN + 25))
                {
                    DAC_PH = (((REC_RATE + 1) << 6) - 1) >> 8; // new rate!!
                    DAC_PL = (((REC_RATE + 1) << 6) - 1) & 0xff;
                    rateAdj = 0; // 1 means tune faster
                    PIOA |= (PA_LEDADJ1 | PA_LEDADJ2);
                    //if (handFree)
                    //  ECCON |= 2;
                }
            }
            else if (rateAdj == 2)
            {
                if (lead < (VOICE_LOAD_LEN + 25))
                {
                    DAC_PH = (((REC_RATE + 1) << 6) - 1) >> 8; // new rate!!
                    DAC_PL = (((REC_RATE + 1) << 6) - 1) & 0xff;
                    rateAdj = 0; // 1 means tune faster
                    PIOA |= (PA_LEDADJ1 | PA_LEDADJ2);
                    //if (handFree)
                    //  ECCON |= 2;
                }
            }
        }
    }

    DCLAMP = 0xF8; // clamp level high and clear!!
}

// Following is the critical part, answer_call()

// Ringing(dialing) and talking use different channels. Dialing channel is fixed.
// Talking channel uses one of the best (lowest rssi ) set of 2 channels sensed by caller
// It is possible the same channel at callee side is bad, we cannot control that.
// We don't complicate the problem.

// The first step is setup the call.
// Callee send the "data" packet at caller's "using(talking) channel", and caller will know callee
// anserred the call.
// The caller then start to send "voice" packet. If callee receive the "voice packet",
// it knows caller sending the voice now. Callee will start send the voice data, too. The call set up.

// Because  packets will loss, we recommend the voice data should be send twice at least.
// That is, MS326 has 256 bytes of rec/pla circular fifo,
// and the voice send/receive 61 bytes per packet. Total payload is 63 bytes,
// MC3100 need a "length" byte before payload. First byte of payload is "voice" tag, and
// second byte is "fifo offset" of the sender.
// However, consecutive packets use fifo offset difference of 30 bytes. That means every byte is
// sent 2 times.
// If single packet loss, it will not be heared.
// Per 30 byte, DMA_IL set the flag position of REC pointer when waiting other to send.
// MS326 have EC (echo cancellation) function, but has not TDD reduce engine. TDD
// is processed at prepare_voice_tx(), which is coded in assembly in "qechk.s",
// and will be placed and RUN in RAM.
// Program runs in RAM is "much" faster than on the SPI flash, especially when branch.
// 2K BYTES RAM of MS326 is still very useful.

// To reduce TDD, ulaw data must be convert back to linear data then substract
// the "estimated" TDD, then convert back
// to ulaw.
// when calls _reduce_tdd
// ROMPTR -> voice data
// hwptr0,1 -> tdd (emulate noise)
// result wrote to ROMPINC directly, no software pointer operation at all.

// If digital mic is used, maybe tdd is not an issue.

// Of course, MS326 can record sound in linear format, the problem is that linear format have the 2x speed to write RAM.
// it is hard to process the sync problem of record and play,
// besides, the RAM seems not very enough
// 0x8000~0x80FF:  zp ram
// 0x8100~0x82cx:  ram used for code, voice_tx, reduce tdd, and interrupt
// 0x8300: EC BUFC (coefficient), it can reduce but ... keep it first
// 0x8400: ULAW recording buf
// 0x8500: play ULAU buf
// 0x8600: EC old data buf
// 0x8700: hardware filter for rec/pla
// current EC is not very long , combined together is possible
// this is only a toy.

// The data caller recorded will be send to callee to play, and vice versa,
// However, different crystal oscillator is used in both side.
// MS326 is hard to fine-tune recording sample rate because the decimation process have /256 clock fixed.
// Only playing rate can be easier adjusted because the filter is operate at DACRATE * 4
// that is, we must adust playing rate for data sync,
// if play too slow, we make it faster for a while
// if play too fast, we make it slower for a while.
// remember EC coef update shall be disabled at that adjust period because T/R using different rate.
// that is what voice_sync() do.
// Because when sync, DMA is still operating, it is hard to write an optimal sync function.
// It generally wrote by try and error.

// the code also shows that when the RF is stable, timing is decide by
// CALLER's ADC receiving process. It sends a packet per 30 samples.
// CALLEE respond a packet by the time it receives data.
// CALEE should send a packet every 30 samples.
// However, it is not necessary the time difference is 30 sample
// because sometimes it is 29 or 31 samples.
// generally the callee will wait 30 samples to send the next packet, or 29
// if it is not always 30 samples, the TDD noise reduction will be in trouble.

// the entire function cannot debug/halt by ICE, it runs in real time.
// previously debugged with A7137 "model" with simulation, but the TDD issue is hard to trace.

#ifndef LINEAR_REC
#error " LINEAR_REC not defined"
#endif
// change to global for check
uChar newTxTime;
uChar chkDiff;

uChar rand8;

void answer_call(uChar caller) // 1 means I am the caller
{
    uChar connected = 0; //
    uChar miss = 10;
    uChar miss2 = 0;
    uChar find_first = 0;
    uChar countpkt = 0;
    //uChar lastTxTime=0;
    uChar needHangup = 0;
    uChar iHangup = 0;
    uChar rxFirst = 1;
    headNow = TAG_VOICE_CHK + 1;

    // for caller, handfree is defined when CPT
    // for callee handfree is 0 initially
    // at beginning, mute all
    api_play_stop();
    DAC_PH = (((REC_RATE + 1) << 6) - 1) >> 8; // new rate!!
    DAC_PL = (((REC_RATE + 1) << 6) - 1) & 0xff;

    timer_ledl = 1;      // when talk, back light is no need
    PIOA &= PA_LEDR_INV; // R should be on when talking

    voicei_ptr = 0;
    voiceo_ptr = 0;
    voice_ptru = 0;
    //ec_state=0; // normal

    // clear RAM 300~7FF, 100~2ff used as code!!
    ROMPLH = (char *)0x8300;
    do
    {
        ROMPINC = 0;
        ROMPINC = 0;
        ROMPINC = 0;
        ROMPINC = 0;
    } while (ROMPH != 0x88);
    SPKC |= 0x20; // bit 5 is HPF enable

    // for data packet, first uChar MSB=1
#if DMI
    micSoftGainNow = DMIGAINHS;
#else
    micSoftGainNow = MICGAIN_ANA;
#endif

    // BOTH need ADC ON
    L2USH = SH_L2U;
    U2LSH = 0;
#if LINEAR_REC
    ULAWC = (ULAWC & 0x0f) | 0x60; // still ulaw playing
#else
    ULAWC |= 0xe0; // NOFF!!
    // adc clamp will disable coef update, we use 80
    DCLAMP = 0xF8; // clamp level high!!
#endif

#if DMI
    // opag hs no use?
    // special setting for digital mic!!
    DMICON = 0x90; // bit 4 means special adjust, check later

    api_rec_prepare(API_ADC_OSR128, 0xf4, 0xff, 1, 0x30, (char *)(REC_STARTA), RBUF16LEN); //
    RCLKDIV = (REC_RATE - 1) >> 1;
    BGCON |= 0x10;
    SYSC2 |= 0x20;
    //DMICON |= 0xC0;// bit 4 means special adjust, check later
#else
    api_rec_prepare(API_ADC_OSR128 | (REC_RATE << 2), OPAG_HS, 0x7f, 1, 0x20, (char *)(REC_STARTA), RBUF16LEN);
#endif
#if !ICE_DBG
    SYSC |= SYSC_BIT_SKCMD; // still skcmd!!
#endif

    // when talking, cpu not check dma interrupt per half page
    // we use DMAI flag
    SYSC2 &= ~SYSC2_BIT_DMAI7;

    // default volume, hs
    delay2nms(10); // the 0.1 second do nothing
    // always use talk channel to handshake!!
    FILTERGP = 0; // initial no sound

    if (!caller)
    {
        // we send data packets to caller say that I will answer the call
        // after a packet is received
        // we make a timeout

        // target address is caller address
        // we check both pipe..

        txNumber = rf_buf[1]; // this is the number from caller
        handFree = 0;         // generally it is 0 initially,
        while (1)
        {
            timer1 = 100;
            next_channel_ce0(2); // for callee check the answer-call channel

            while (RFINTPIN && timer1)
                ;
            if (!timer1) // answer call timeout
            {
                warmRst();
            }
            if (read_packet(1) == 0)
            {
                timer1 = 2;
                if ((rf_buf[0] & TAG_VOICE_MASK) == TAG_VOICE_CHK) // channel 0 means speech data comes
                    // we start to receive voice data now
                {
                    RDMAH = 0x80;
                    PDMAH = 0x80;
                    next_channel_ce0(2);
                    voice_ptru = (256 - VOICE_LOAD_LEN + 3) & 0xff; // first assume 3

                    break;
                }
                else // caller still wait my msg
                {
                    outid[0] = rf_buf[1];
                    outid[1] = rf_buf[2];
                    outid[2] = rf_buf[3];
                    outid[3] = rf_buf[4]; // this uChar no change
                    SPI_Write_Strobe(CMD_STBY);
                    SPI_Write_Reg(CODE3_REG, txNumber);
                    ROMPLH = rf_buf; // send back samedata
                    SPI_Write_Buf_RAM(FIFO_REG);
                    delay2nms(1);  // delay 2ms
                    out_packet(0); // no change channel
                }
            }
            next_channel_ce0(2); // change channel
            // for the callee freq change after rx
        }
        // if pipe 0 received, we start sync
        // read pipe0 to my fifo first

        // tma clock is fsys/8 /256 = fsys/2048
        // adc clock is fsys/[(RATE+1)*2*OSR*INC]
        EA = 0;
        //THRLD=255-(REC_RATE+1)*VOICE_PTR_INC/8; // use timer to see if timeout
        while (1)
        {

            // try to sync
            // we try to debug here
            // comes here means the caller has send speech data,
            // and we will continue send speech data, too
            // change to ptx mode
            SPI_Write_Strobe(CMD_STBY);
            SPI_Write_Reg(CODE3_REG, txNumber);
#if LINEAR_REC
            newTxTime = ((voice_ptru << 1) + VOICE_PTR_INCx2);
#else
            newTxTime = voice_ptru + VOICE_PTR_INC;
#endif
            if (!miss)
            {
#if LINEAR_REC
                chkDiff = ((uChar)(((unsigned short)RDMALH) >> 1)) - voice_ptru;
                if (chkDiff < (VOICE_PTR_INC - 2)) // reserve 1 for check
                    newTxTime -= 2;
                if (chkDiff > VOICE_PTR_INC)
                    newTxTime += 2;
#else
                chkDiff = RDMAL - voice_ptru; // voiceptru set in prepare...

                if (chkDiff < (VOICE_PTR_INC - 2)) // reserve 1 for check
                    newTxTime--;
                if (chkDiff > VOICE_PTR_INC)
                    newTxTime++;
#endif
            }
            if (miss < 2)
            {
                while (RDMAL != newTxTime)
                    ;
            }
            prepare_voice_tx(); //no retran voice_ptr, chk_ptr update in it!!

            // sync the output to 20 sample each
            if (out_packet(4)) // callee change channel after pkt received
            // after last step, change back to prx mode.
            {
                if (!needHangup)
                    needHangup = 1;
                FILTERGP = 0;
                iHangup = 1;
                headNow = TAG_HANGUP;
            }
        skip_send:
            if (miss == 128)
            {
                //miss2++;
                if (++miss2 == MISSMAX)
                    break;
                miss = 0;
            }
            else if (miss == 4)
            {
                // mute output
                //clear_play_buf();
                FILTERGP = 0;
            }
            TOV = 0;
            // if(!miss)
            // {
            //     TOV=0;
            //     while (RFINTPIN && (!TOV)) ;
            // }
            // else
            // 2020 feb, already a buffer
            // #if LINEAR_REC
            //             DMA_IL-=2;
            // #else
            //             DMA_IL--; // give a sample for buffering
            // #endif
            while (RFINTPIN && (!(GIF & 0x80)))
                ; // if miss we wait master!!

            if (!RFINTPIN)
            {

                //TIMERC=7; // reset it
                if (read_voice_packet() == TAG_HANGUP)
                {
                    if (needHangup != 255)
                        needHangup++;
                    else
                    {
                        break;
                    }

                    headNow = TAG_HANGUP;
                    FILTERGP = 0;
                }
                next_channel_ce0(2);
                miss = 0;
                miss2 = 0;
                if (countpkt != 255)
                    ++countpkt;
                voice_sync(countpkt);
            }
            else
            {
                // stop rate adj
                if (miss2 && needHangup)
                    break;
                miss++;
                if (rateAdj)
                {
                    rateAdj = 0;
                    DAC_PH = (((REC_RATE + 1) << 6) - 1) >> 8; // new rate!!
                    DAC_PL = (((REC_RATE + 1) << 6) - 1) & 0xff;

                    PIOA |= (PA_LEDADJ1 | PA_LEDADJ2);
                    //if (handFree)
                    //ECCON |= 2;
                }
                if (miss < 5) // no change when cont miss
                {
                    next_channel_ce0(2); // we check next next freq
#if LINEAR_REC
                    DMA_IL += VOICE_PTR_INCx2;
#else
                    DMA_IL += VOICE_PTR_INC;
#endif
                    GIF = 0x1F;
                }
                else
                {
                    if ((miss & 31) == 0)
                        next_channel_ce0(2);
                        // DMA_IL will be modified when prepare..
#if LINEAR_REC
                    DMA_IL += VOICE_PTR_INCx2;
#else
                    DMA_IL += VOICE_PTR_INC;
#endif
                    GIF = 0x1F;

                    goto skip_send;
                }
            }
        }
    }
    else // here is caller
    {    // we start send data
        // change back to channel0
        // caller change channel after send, callee change channel after rec

        // 8 uChars a time
        // until 0 received
        next_channel_ce0(1); // for caller check the call channel
        // since interrupt may
        EA = 0;
#if LINEAR_REC
        while (!(RDMAH & 0x01))
#else
        while (!(RDMAL & 0x80))
#endif
            ; // we wait half page, fine
        while (1)
        {
            SPI_Write_Strobe(CMD_STBY);
            SPI_Write_Reg(CODE3_REG, txNumber);

            prepare_voice_tx();        // change to no ack
            if (out_packet(1 + 1 + 4)) // for the caller .. change freq at talk mode
            {
                if (!needHangup)
                    needHangup = 1;
                FILTERGP = 0;
                iHangup = 1;
                headNow = TAG_HANGUP;
            }
            if (miss == 128)
            {
                miss = 0;
                if (++miss2 == MISSMAX)
                    break; // give busy tone
            }
            else if (miss == 4)
            {
                // mute output
                //clear_play_buf();
                FILTERGP = 0;
            }
            miss++;
            while (RFINTPIN && !(GIF & 0x80))
                ; // just wait is fine

            if (!RFINTPIN)
            {
                if (read_voice_packet() == TAG_HANGUP)
                {
                    if (needHangup != 255)
                        needHangup++;
                    else
                    {
                        break;
                    }

                    headNow = TAG_HANGUP;
                    FILTERGP = 0;
                }

                miss2 = 0;
                miss = 0;
                if (countpkt != 255)
                    ++countpkt;
                voice_sync(countpkt);
                while (!(GIF & 0x80))
                    ;
            }
            else
            {
                if (miss2 && needHangup)
                    break;
                if (rateAdj) // if no receive , no rage adj
                {
                    rateAdj = 0;
                    DAC_PH = (((REC_RATE + 1) << 6) - 1) >> 8; // new rate!!
                    DAC_PL = (((REC_RATE + 1) << 6) - 1) & 0xff;

                    PIOA |= (PA_LEDADJ1 | PA_LEDADJ2);
                    //if (handFree)
                    //ECCON |= 2;
                }
            }
        }
    }
    flushtr();
    FILTERGP = 0;
    ECCON = 0;
    SPI_Write_Strobe(CMD_RX);
    api_play_stop();
    api_rec_stop(0);
    if (handFree || iHangup)
    {
        api_set_vol(0x3f, playVolTabHF[volHF]);
    }
    else
    {
        api_set_vol(HSPAG, playVolTabHS[volHS]);
#if HANDSETPA7
        API_PSTARTH_PA7(bye12k);
#else
        API_PSTARTH(bye12k);
#endif
    }

    while (api_play_job())
        api_enter_stdby_mode(0, 0, 0, 0, 0);
    api_play_stop();

    warmRst();
}

uChar out_packet(uChar changechannel)
{
    uChar needHangUp = 0;
    WDTH = 0x40; // time to OV
    WDTL = 0x5;  // clear and enable interrupt
    if (changechannel & 4)
    {
        shift_voice_buf();
    }
    else
        SPI_Write_Strobe(CMD_TX);

    if (!(GIE & 0x80)) // EA cannot use LDC
    {
        needHangUp = keyud_chk();
        if (handFree && hfTraining)
        {
            // headnow modify
            --hfTraining;
            headNow = TAG_VOICE_CHK | 0x03; // special tag
        }
        else //if(voicei_ptr<VOICE_PTR_INC)
        {
            adjHeadNow();
        }
    }
    while (RFINTPIN)
        ; // TX should be very fast, need not check dmaif

    WDTL = 0; // disble
    SPI_Write_Strobe(CMD_STBY);
    SPI_Write_Reg(CODE3_REG, myId);
    flushtr();
    if (changechannel & 3)
        next_channel_ce0((changechannel & 3) - 1);
    else
        SPI_Write_Strobe(CMD_RX);
    return needHangUp;
}

// prepare_voice_tx moved to assembly, in qechk.s
#ifdef USEC1
#if SUPRESSTDD
short tmpshort;
#endif
void prepare_voice_tx(void) // input is ramp0,out tospi
{
    // it is tricky that per packet we increase 10 samples
    // we store last
    // 2 stages

    // 2017 change to single stage
    // recording is 256 uChar
    // we loop
    // since ulaw directly,
    // we need not

    uChar len1;
    //#ifdef SUPRESSTDD
    //	uChar i;
    //#endif
    voice_ptru = RDMAL;
    voiceo_ptr = voice_ptru - VOICE_LOAD_LEN; // ulaw domain, overflow is ok
    //ROMPL=voiceo_ptr;

#ifdef SUPRESSTDD
    ROMPL = voiceo_ptr;
    HWPSEL = 0;
    HWPALH = (char *)&tdd;
    HWPSEL = 1;
    __asm LDA #high(_tdd + 0);
    STA(_HWPALH + 1);
    LDA #(_tdd + 0);
    STA _HWPALH;
    call _reduce_tdd;
    __endasm;

#endif

    cslow();
    SPIDAT = FIFO_REG;   // voice always no ack
    SPIDAT = TAG_VOICE;  // first uChar is VOICE-TAG!!
    SPIDAT = voiceo_ptr; // this is uChar basis!! 64-2=62

    // debug use special frame
    SDMAAH = REC_BUFAHDBG;
    SDMAAL = voiceo_ptr;

    // consider if load len = 5
    // output need sep if
    // vioceo==252 253 254 255 0
    //         253
    //  .......255
    // which means < 252 in 1 piece
    // >= 252 in 2 pieces, 252 is 257-5

    if (voiceo_ptr < 257 - VOICE_LOAD_LEN) // only 1 time transfer
    {
        SDMALEN = VOICE_LOAD_LEN - 1;
        SPIDMAC = 0x02; // write only
        while (SPIDMAC & 0x80)
            ;
    }
    else
    { // 2 parts
        len1 = 256 - voiceo_ptr;
        SDMALEN = len1 - 1;
        SPIDMAC = 0x02;
        while (SPIDMAC & 0x80)
            ;
        // second part
        SDMALEN = VOICE_LOAD_LEN - len1 - 1;
        SDMAAH = REC_BUFAHDBG;
        SDMAAL = 0;
        SPIDMAC = 2;
        while (SPIDMAC & 0x80)
            ;
    }

    cshigh();

    DMA_IL = voiceo_ptr + VOICE_PTR_INC + (PAYLOADLEN - 2);
    GIF = 0x1f; // always clear flags
}
#endif
void number_key(void)
{
    uChar number;

    if (sys_stage > 2 || (sys_state == SYS_DIAL && sys_stage == 2) || sys_state == SYS_RING)
    {
        key_code = 0;
        return;
    }
    if (key_code == KEY_CODE_0)
        number = 0;
    else
        number = key_code - 3;
    key_code = 0; // now ready for next key
    sw_to_osc();
    api_set_vol(0x3f, playVolTabHF[volRing]);
    switch (number)
    {
    case 0:
        API_PSTARTH(zero);
        break;
    case 1:
        API_PSTARTH(one);
        break;
    case 2:
        API_PSTARTH(two);
        break;
    case 3:
        API_PSTARTH(three);
        break;
    case 4:
        API_PSTARTH(four);
        break;
    case 5:
        API_PSTARTH(five);
        break;
    case 6:
        API_PSTARTH(six);
        break;
    case 7:
        API_PSTARTH(seven);
        break;
    case 8:
        API_PSTARTH(eight);
        break;
    case 9:
        API_PSTARTH(nine);
        break;
    }
    play_to_finish();
    timer1 = 10;               // we wait 5 seconds for dialing, etc.
    if (sys_state == SYS_IDLE) // first beep already in idle
    {
        outid[0] = number;
        sys_state = SYS_DIAL;
        sys_stage = 0;
        return;
    }
    else if (sys_state == SYS_DIAL)
    {
        //dbg_uChar(number);
        //dbg_uChar(sys_stage);
        //if (sys_stage >= 2)
        //return;

        PIOA &= PA_LEDR_INV;
        //        beep_short(KEY_BEEP_LEN);
        PIOA |= PA_LEDR;
        sys_stage++;
        ROMPLH = &outid[sys_stage];
        ROMP = number;

        if (sys_stage == 2)
        {
            txNumber = (outid[0] * 100 + outid[1] * 10 + outid[2]) & 0x7f;
            SPI_Write_Strobe(CMD_STBY);
            // dial out here!!
            // start cpt
            delay2nms(2);
            sw_to_osc();

            // need to decide dial talk channel, in 0.5S
            channelGroup = selectDialOutChannel();

            mld_timer = DIALLEN;

            ringcount = 0;
            progressuChar = PDIALING;
            RX0_Address[0] = myId;
            // channel it will use
            RX0_Address[1] = usingChannel[channelGroup << 1];
            RX0_Address[2] = usingChannel[(channelGroup << 1) + 1];

            //ROMPLH=outid; // dialing ID is outid

            // change id
            SPI_Write_Reg(CODE3_REG, txNumber); // change seed!!
            SENDTAG = TAG_DATA;
            ROMPLH = &SENDTAG; // SKIP LATER DATA IS FINE!!
            SPI_Write_Buf_RAM(FIFO_REG);
            out_packet(1); // the caller changes channel, broadcast
            // after out_packet, ce is high, primary RX!!
            TIMERC = 3;          // reset timer
            timer_wait_call = 2; // check reply in 4ms
            timer1 = 33;         // dial timeout is 16 second, timer1 -=1 per 0.5 second

            // cpt out after switch to osc
            handFree = 0; // initial is not handfree!!
            PIOA &= PA_LEDR_INV;
            if (channelGroup & 1)
                PIOA &= PA_LEDADJ1_INV;
            if (channelGroup & 2)
                PIOA &= PA_LEDADJ2_INV;

            api_set_vol(HSPAG, playVolTabHS[volHS]);
#if HANDSETPA7
            API_PSTARTH_PA7(cpt);
#else
            API_PSTARTH(cpt);
#endif
        }
    }
    else if (sys_state == SYS_SYNC)
    {
        //beep_short(80);
        timer1 = SYNC_TIMEOUT * 2;
        if (sys_stage < 3)
        {
            ROMPLH = &outid[sys_stage]; // array is buggy?
            ROMP = number;
            sys_stage++;
        }
    }
}

void beep_short(uChar len)
{
    //beep_timer=len;
    uChar volsave = FILTERGP;
    uChar daconsave = DACON;
    uChar pagsave = PAG;
    api_set_vol(0x3f, playVolTabHF[volRing]);
    api_beep_start(0x20, 4095);
    timer0 = len;
    while (timer0)
        api_enter_stdby_mode(0, 0, 0, 0, 0);
    api_beep_stop();
    api_set_vol(pagsave, volsave);
    DACON = daconsave;
}
// ms326 have no dual-channel dac
// it has only single dac with filter
// however, play a seg from spi flash is good

void set_new_addr(void)
{
    myId = (outid[0] * 100 + outid[1] * 10 + outid[2]) & 127;
}
void resetmldt(uChar newmld)
{
    if (!mld_timer)
    {
        PIOA ^= PA_LEDR; // LEDR is ok
        mld_timer = newmld;
    }
}

void sw_to_osc(void)
{
    //clear_play_buf();
    if (RCCON != 0x09)
    {
        RCCON = 0x25;
        RCCON = 0x29;
        RCCON = 0x09;
    }
}

void warmRst(void)
{
    // we copy the data to 4xx and add marks
    //backup_ram();
    WDTL = 0x04; // clear first
    WDTH = 0x08;
    //WDTL = 0x3; // now enable wait reset
    ROMPLH = (uChar *)0x87f0;
    ROMPINC = 0x55;
    ROMPINC = 0xaa;
    ROMPINC = 0xbb;
    ROMPINC = 0xdd;
    __asm
        .DW 0xffff
        .DW 0xffff __endasm;
    while (1)
        api_enter_stdby_mode(0, 0, 0, 0, 1);
    //API_USE_IRC;
    //longjmp(restartLabel, 1);
}

// void set_specialf(uChar f)
// {
//     specialf = f;
//     //backup_ram();
// }
void flushtr(void)
{
    SPI_Write_Strobe(CMD_TFR);
    SPI_Write_Strobe(CMD_RFR);
}

// void findGoodChannel(void)
// {
//     // 2401~2475 there are 150 channels, we check 50 channels
//     // step 3
//     // result at 4XX
//     uChar i, j, tmp;
//     for (tmp = 0; tmp < 5; tmp++) // 5 times
//     {
//         for (i = 0; i < 50; i++)
//         {
//             j = i * 3 + 1;
//             SPI_Write_Reg(PLL1_REG, j);
//             rssi_ch[i] = j;
//             rssi_val[i] += getRSSI();
//         }
//     }
//     for (i = 49; i != 0; i--)
//     {
//         for (j = 0; j < i; j++)
//         {
//             if (rssi_val[j] > rssi_val[j + 1])
//             {
//                 tmp = rssi_val[j];
//                 rssi_val[j] = rssi_val[j + 1];
//                 rssi_val[j + 1] = tmp;
//                 tmp = rssi_ch[j];
//                 rssi_ch[j] = rssi_ch[j + 1];
//                 rssi_ch[j + 1] = tmp;
//             }
//         }
//     }
// }

// void test_full_duplex_mode(void)
// {
// 	// after beep, we check if key released
// 	// 8600~87ff is for dac play+filter
// 	// rec .. we use 8100~85ff
//     uChar j;
// #if DMI
//     // opag hs no use?
//     // special setting for digital mic!!
//     DMICON = 0x90; // bit 4 means special adjust, check later

//     api_rec_prepare(API_ADC_OSR128, 0xf4, 0xff, 1, 0x30, (char *)(REC_STARTA), RBUF16LEN); //
//     RCLKDIV = 3;
//     BGCON |= 0x10;
//     SYSC2 |= 0x20;
//     //DMICON |= 0xC0;// bit 4 means special adjust, check later
// #else
//     api_rec_prepare(API_ADC_OSR128 | (REC_RATE << 2), OPAG_HS, 0x7f, 1, 0x20, (char *)(REC_STARTA), RBUF16LEN);
// #endif

//     while(1)
//     {
// #if DMI
//     if(RECPWR>0x1000L)
//         break;
// #else
//     if(RECPWR>0x100000L)
//         break;
// #endif
//     api_enter_stdby_mode(0,0,0,0,0);

//     }
//     beep_short(200);
//     return;

// }

void do_test(void)
{
    uChar i;
    beep_short(100);
    for (i = 1; i <= 15; i++)
    {
        while (key_code != i)
        {
            key_machine();
            if (key_state)
                api_enter_stdby_mode(0, 0, 0x0, 0, 0);
            else
                api_enter_stdby_mode(0, 0, 0x1f, 0, 0);
            if (key_code != 0)
            {

                beep_short(40);
                if (key_code != i)
                    key_code = 0;
            }
        }
        delay2nms(20);
        beep_short(50);
        key_code = 0;
    }
    // full duplex
    //     delay2nms(20);
    //     beep_short(50);
    //     delay2nms(200);
    // test_full_duplex_mode();
    //     delay2nms(20);
    //     beep_short(50);
    //     delay2nms(200);

    // finally , tx at
    key_code = 0;

    while ((PIOB & 0x1f) == 0x1f)
    {

        SPI_Write_Strobe(CMD_STBY);
        progressuChar = PDIALING;
        RX0_Address[0] = myId;
        // channel it will use
        RX0_Address[1] = usingChannel[channelGroup << 1];
        RX0_Address[2] = usingChannel[(channelGroup << 1) + 1];

        SPI_Write_Reg(CODE3_REG, 0x55); // change seed!!
        SENDTAG = TAG_DATA;
        ROMPLH = &SENDTAG; // SKIP LATER DATA IS FINE!!
        SPI_Write_Buf_RAM(FIFO_REG);
        out_packet(0); // test don't change channel
        beep_short(20);
        delay2nms(200);
    }

    beep_short(200);
    delay2nms(100);
    beep_short(200);
    warmRst();
}

void play_to_finish(void)
{
    uChar lastCode;
    while (api_play_job())
    {
        if (need_key || (key_state == 0 && ((PIOB & 0x1f) != 0x1f)))
        {
            lastCode = key_code;
            key_machine();
            need_key = 0;
            if (key_code && key_code != lastCode)
                break;
        }
        if (key_state)
            api_enter_stdby_mode(0, 0, 0x0, 0, 0);
        else
            api_enter_stdby_mode(0, 0, 0x1f, 0, 0);
    }
    api_play_stop();
}
uChar restart;

uChar checkrestart(void)
{
    ROMPLH = (uChar *)0x87f0;
    if (ROMPINC == 0x55 && ROMPINC == 0xaa &&
        ROMPINC == 0xbb && ROMPINC == 0xdd)
    {
        ROMPLH = (uChar *)0x87f0;
        ROMPINC = 0;
        return 1;
    }
    return 0;
}
void main(void)
{
    // for simulation easy
    //STACKH = 0x7f;
    //STACKL = 0x55;

    // restart = setjmp(restartLabel);
    restart = checkrestart();
    init();
#if !SIMULATION
    if (!restart)
    {
        RCCON = 0x45; // use div2 clock to save power
        delay2nms(100);
        delay2nms(100);
        RCCON = 0x05;
    }
#else
    // we test multiplier
    // MULSHIFT = 2; // shift
    // PIOA &= 0xbf;
    // MULA =  1000;
    // PIOA |= 0x20;
    // MULB = 154;
    // if(MULO!=2*154000L)
    // {
    //     while(1)
    //     {
    //         PIOA &= 0xbf;
    //         PIOA |= 0x20;
    //     }
    // }
    //     PIOA &= 0xbf;
    // MULA =  -1000;
    // PIOA |= 0x20;
    // MULB = 154;
    // if(MULO!=-2*154000L)
    // {
    //     while(1)
    //     {
    //         PIOA &= 0xbf;
    //         PIOA |= 0x20;
    //     }
    // }

    // MULSHIFT=0;

#endif
    // RF init change to 3 steps
    rf_reset();
    set_rf_id(); // use 1 for talk

    rf_config();
    rf_cal();
    SPI_Write_Reg(CODE3_REG, myId);
    SPI_Write_Strobe(CMD_STBY); // stdby mode first
    cslow();                    // set length=64 uChars
    SPIDAT = FIFO1_REG;
    SPIDAT = 0x3f;
    SPIDAT = 0x00;
    cshigh();
    flushtr();
    //findGoodChannel();
    SPI_Write_Strobe(CMD_RX); // we enter rx mode
    key_code = 0;
#if !SIMULATION
    if (!restart)
    {
        sw_to_osc();
#ifndef POWERON_LOUD
#error("POWERON_LOUD NOT DEFINED")
#endif

#if POWERON_LOUD
        api_set_vol(0x3f, playVolTabHF[0]);
#else
        api_set_vol(0x3f, playVolTabHF[volRing]);
#endif
        API_PSTARTH(hellochina);
        key_code = 0;
        play_to_finish();
        API_USE_IRC;
    }
#endif
loopmain:

    // key functions
    if (need_key || (key_state == 0 && ((PIOB & 0x1f) != 0x1f)))
    {
        key_machine();
        need_key = 0;
    }
    if (key_code)
    {

        if (sys_state == SYS_SYNC && sys_stage >= 3)
        {
            enter_idle_mode();
            key_code = 0;
        }
        timer_ledl = 1; // back light on
        PIOA &= PA_LEDL_INV;
        switch (key_code)
        {
            //        case 0:
            //            break;
        case KEY_CODE_TEST:
            key_code = 0;
            if (sys_state != SYS_IDLE)
                break;
            do_test();
            break;

        case KEY_CODE_TALK:
            key_code = 0;
            if (sys_state == SYS_RING)
                answer_call(0);
            else
            {
                beep2ifNotIdle();
                enter_idle_mode();
            }
            break;
        case KEY_CODE_ONOFF: // TAG: ONOFF
        sleep_again:
            beep2ifNotIdle();
            //set_specialf(1);
            specialf = 1;
            enter_idle_mode();
            specialf = 0;
            //set_specialf(0);
            // on off key is pa2 pa4
            PIOA |= PA_LED_ALL; // all OFF
            SPI_Write_Strobe(CMD_SLEEP);
            while (get_key())
                ;

            // delay a while
            delay2nms(200);

            WDTL = 4; // clear the flags..really sleep here
            WDTH = 0;
            sleep(1); // here we use pb0 wakeup
            timer_ledl = 0;
            do
            {
                delay2nms(100);
                if (get_key() != KEY_CODE_ONOFF)
                    goto sleep_again;
            } while (++timer_ledl < 5);

            PIOA &= PA_LEDR_INV; // sleep will turn it on
            PIOA |= PA_LEDL;
            timer_ledl = 6; // on again turn on 3 seconds
            key_code = 0;
            break;
        case KEY_CODE_HANGUP:
            if (sys_state == SYS_RING)
            {
                sys_stage = 2; // force no ring, wait next call
            }
            else
            {
                if (sys_state != SYS_DIAL)
                    beep2ifNotIdle();
                enter_idle_mode();
            }
            key_code = 0;
            break;
        case KEY_CODE_0:
        case KEY_CODE_1:
        case KEY_CODE_2:
        case KEY_CODE_3:
        case KEY_CODE_4:
        case KEY_CODE_5:
        case KEY_CODE_6:
        case KEY_CODE_7:
        case KEY_CODE_8:
        case KEY_CODE_9:
            number_key(); // clear key code in number_key
            break;
        case KEY_CODE_DN:
            vol_dn();
            if (sys_state == SYS_IDLE)
            {
                enter_ringing_mode();
            }
            else if (sys_state == SYS_RING)
            {
                ringcount = 0;
                sys_stage = 0;
                if (timer1 < 4)
                    timer1 = 4;
            }
            else if (sys_state != SYS_DIAL)
            {
                beep2ifNotIdle();
                enter_idle_mode();
            }
            key_code = 0;
            break;
        case KEY_CODE_UP:
            vol_up();
            if (sys_state == SYS_IDLE)
            {
                enter_ringing_mode();
                timer1 = 4;
            }
            else if (sys_state == SYS_RING)
            {
                ringcount = 0;
                sys_stage = 0;
                if (timer1 < 4)
                    timer1 = 4;
            }
            else if (sys_state != SYS_DIAL)
            {
                beep2ifNotIdle();
                enter_idle_mode();
            }
            key_code = 0;
            break;
        case KEY_CODE_SYNC:
            enter_idle_mode();
            sys_state = SYS_SYNC;
            sys_stage = 0;
            //sync_count = 0;

            sw_to_osc();
            api_set_vol(0x3f, playVolTabHF[volRing]);
            API_PSTARTH(settingnow);
            play_to_finish();

            timer1 = (SYNC_TIMEOUT * 2); // get 30 seconds
            key_code = 0;
            break;
        case KEY_CODE_HF:
            if (sys_stage >= 2 && sys_state == SYS_DIAL)
                handFree ^= 1;
            if (handFree)
                api_set_vol(0x3f, playVolTabHF[volHF]);
            else
                api_set_vol(HSPAG, playVolTabHS[volHS]);
            key_code = 0;
            break;
        default:
            key_code = 0;
            break;
        }

        //backup_ram(); // for safety
    }
    // sys functions
    if (sys_state >= SYS_SYNC)
    {
        if (!timer1)
        {
            enter_idle_mode();
            goto loopmain;
        }

        if (sys_stage < 3)
        {
            resetmldt(MLD10);

            WDTL = 1; // turn ON WDT only, for randnumber
        }
        else if (sys_stage == 3)
        {

            api_set_vol(0x3f, playVolTabHF[volRing]);

            API_PSTARTH(settingok);
            play_to_finish();
            timer1 = 0;
            set_new_addr();
            enter_idle_mode(); // when enter idle mode, new addr will be set
            goto loopmain;
        }
        else
        {
            resetmldt(5);
        }
    }
    else if (sys_state >= SYS_RING)
    {
#ifdef DEBUG_RING
        timer1 = 4;
#else
        if (!RFINTPIN)
        {
            if (read_packet(1) == 0 && rf_buf[0] == TAG_DATA &&
                rf_buf[6] == PDIALING)
            {
                // copy the first data to tx address
                // rf_buf[1] is sender address
                answerChannel[0] = rf_buf[2];
                answerChannel[1] = rf_buf[3];
                timer1 = 4; // in 2 seconds, we need another packet
            }
            next_channel_ce0(0); // toggle CE again
        }
        if (!timer1)
        {
            api_play_stop();
            enter_idle_mode();
        }
#endif
        if (!api_play_job())
        {
            api_play_stop();
            if (sys_stage < 2)
                API_PSTARTH(incoming); // repeat again
        }
#if GIO1INV
        api_enter_stdby_mode(0, 0, 0x3f, 0x20, 0);
#else
        api_enter_stdby_mode(0, 0, 0x3f, 0, 0);
#endif
        if (mld_timer)
            goto loopmain;

        mld_timer = RINGPULSELEN; // change from 6 to 12 if 16mhz
        PIOA |= PA_LEDR;

        if (++ringcount == 20) // around 2 seconds
        {
            sys_stage ^= 1;
            ringcount = 0;
        }

        if (!sys_stage) // 0 means ring
        {
            if (ringcount & 1)
            {
                // off then on

                PIOA &= PA_LEDR_INV;
            }
            else
            {
                //MLD1=0;
                PIOA |= PA_LEDR;
            }
        }
        //break;
    }
    else if (sys_state) //dial=1, tag: sys_dialing
    {
        // sys dialing by number-key
        if (!timer1) // at final dialing, timer1 set to 33=16.5 seconds
        {
            api_play_stop();
            enter_idle_mode();
            goto stdbyLoopMain;
        }

        if (sys_stage < 2) // wait pressing number
            goto stdbyLoopMain;
        // here is after sent packet, stage=2
        // int low
        if (!RFINTPIN) // if there is interrupt coming, we make the call
        {
            if (read_packet(1) == 0 && rf_buf[0] == TAG_DATA) // if ack is pipe1
                answer_call(1);                               // same function, I am the caller
        }
        else if (!timer_wait_call) // if no ack after 4ms, we send again
        {

            SPI_Write_Strobe(CMD_STBY);
            SPI_Write_Reg(CODE3_REG, txNumber);

            ROMPLH = &SENDTAG; // SKIP LATER DATA IS FINE!!
            SPI_Write_Buf_RAM(FIFO_REG);

            SPI_Write_Reg(PLL1_REG, dialingChannel[channelIndex]);
            SPI_Write_Strobe(CMD_TX);

            while (RFINTPIN)
                ;
            ; // TX should be very fast, need not check dmaif

            next_channel_ce0(1); // STDBY in function
            SPI_Write_Strobe(CMD_STBY);

            ROMPLH = &SENDTAG; // SKIP LATER DATA IS FINE!!
            SPI_Write_Buf_RAM(FIFO_REG);

            out_packet(0);       // no change
            timer_wait_call = 2; // timerwaitcall per 2ms a sent ==> total 2.6ms per channel
            // that is, 4 channel needs 12 ms, 20 channel needs .. more
            need_key = 1; // what ever, set the timer
            TIMERC = 3;   // clear timer to check 2ms skip this will be faster, not af
        }

        if (sys_stage == 2)
        {
            if (api_play_job() == 0)
            {
                api_play_stop();
                // silence interval: 32Hz*2
                mld_timer = RINGINTER; // from 160 to 255 no more faster in 16mhz
                sys_stage = 3;
                PIOA ^= PA_LEDR;
            }
        lmain2:
#if GIO1INV
            api_enter_stdby_mode(0, 0, 0x3f, 0x20, 0);
#else
            api_enter_stdby_mode(0, 0, 0x3f, 0, 0);
#endif
            goto loopmain;
        }
        else if (sys_stage == 3 && !mld_timer)
        {
#if HANDSETPA7
            API_PSTARTH_PA7(cpt);
#else
            API_PSTARTH(cpt);
#endif
            PIOA ^= PA_LEDR;
            sys_stage = 2;
        }
        goto lmain2;
    stdbyLoopMain:
        if (key_state)
            api_enter_stdby_mode(0, 0, 0, 0, 0);
        else
            api_enter_stdby_mode(0, 0, 0x1f, 0, 0);
        goto loopmain;
    }
    else // here is idle state
    {
        if (specialf & 1)
            key_code = KEY_CODE_ONOFF;
        if (!timer_ledl)
            PIOA |= PA_LEDL;
        if (!RFINTPIN)
        {
            if (read_packet(1) == 0 && rf_buf[0] == TAG_DATA &&
                rf_buf[6] == PDIALING)
            {
                enter_ringing_mode();
                next_channel_ce0(0);
                goto loopmain;
            }
        }

        if (!timersleep)
        {
            key_code = KEY_CODE_ONOFF; // power it off!!
            //break;
            goto loopmain;
        }
        // poweron, delay 12ms to see if packet received ....
        // otherwise sleep with wdt wake up

        if (!(timer_wait_call | timer_ledl | timer1 | key_state))
        {
            // if (++count15minl == 120)
            // {
            //     count15minl = 0;
            //     if (count15minh)
            //         --count15minh;
            // }
#if SIMULATION
            WDTH = 0x04; // faster!!
#else
            WDTH = 0x40;
#endif

            WDTL = 0X05; //

            sleep(0); // here we don't use PA4 wake up

            next_channel_ce0(0); //wake up don't change channel??
        }

        api_enter_stdby_mode(0, 0, 0, 0, 0); // keep timer
    }

    goto loopmain;
}

void vol_up(void)
{
    if ((sys_state == SYS_DIAL && sys_stage >= 2) ||
        !(GIE & 0x80))
    {
        if (handFree)
        {
            if (volHF)
                --volHF;
            api_set_vol(0x3f, playVolTabHF[volHF]);
        }
        else
        {
            if (volHS)
                --volHS;
            api_set_vol(HSPAG, playVolTabHS[volHS]);
        }
    }
    else
    {
        if (volRing)
            --volRing;
        api_set_vol(0x3f, playVolTabHF[volRing]);
    }
}
void vol_dn(void)
{
    if ((sys_state == SYS_DIAL && sys_stage >= 2) ||
        !(GIE & 0x80))
    {
        if (handFree)
        {
            if (volHF < TALKVOLMAX)
                ++volHF;
            api_set_vol(0x3f, playVolTabHF[volHF]);
        }
        else
        {
            if (volHS < TALKVOLMAX)
                ++volHS;
            api_set_vol(HSPAG, playVolTabHS[volHS]);
        }
    }
    else
    {
        if (volRing < TALKVOLMAX)
            ++volRing;
        api_set_vol(0x3f, playVolTabHF[volRing]);
    }
}

#ifndef __MS233DEF__
#define __MS233DEF__

// options

// now use -- A7196
#define A7196 1
#ifndef SIMULATION
#define SIMULATION 0
#endif

#ifndef MBPS1
#define MBPS1 0
#endif

// 2019, PIOA key scan use pioa345
#ifndef PAKEY345
#define PAKEY345 0
#endif 

#ifndef HANDSETPA7
#define HANDSETPA7 0
#endif

#ifndef DMI
#define DMI 0
#endif

#if DMI
#define DMIGAINHS 120
#define DMIGAINHF 180
#define SH_L2U 2
#else
#define MICGAIN_ANA 128
#define SH_L2U 0

#endif
#define INI_FGP 5
#define ECCON_DEFAULT 0x23

#define GIO1INV 1

#define ICE_DBG 0
#define EC_EN 1
#define EC_LEN 48
#define CRC_EN 1
// OP GAIN
#define OPAG_HS 0x40
#define OPAG_HF 0x90
// osr 128
// try lower rate
// 11: 5.2k
// 9: 6.2k
// 7: 7.8K
// 6: 8.9k
// 5: 10.4k
#if MBPS1
#define REC_RATE 9
#else
#define REC_RATE 7
#endif

#define OSC16MHZ 1
#define OSC32MHZ 0

#define CPU16MHZ 1
#define SUPRESSTDD 0
#define USE_ERC 0
#define SLEEP2S 0
#define SINGLECHAN 0
#define AVOIDWIFI 0


#define HOP_NUM 2
#define HOPINDEX_MAX 1
#define DEFAULT_CHAN_ST 148
#define DEFAULT_CHAN_INC 163

#define PAYLOADLEN 64

// formerly 14+ hop#, now rx1 addr is removed, change to 9+h#
//#define RAMSAVEuChar (9+HOP_NUM)

// for cheap version
// the hopping sequence is not changed



#if SYNC_CHANGE_HOP
#define SYNC_TIMEOUT 30
#else
#define SYNC_TIMEOUT 6
#endif

#define LINEAR_REC 1

#if CPU16MHZ

// note!! change voice_ptr_inc need to change
// the value in QECHK.s !!!!
// QECHK.s need also change !!!
#define VOICE_PTR_INC 30 // we use slow one, 30!! 21*3=63
#define VOICE_PTR_INCx2 60

// if this value changed, qechk.s need to change, too.

//#define VOICE_PTR_INCx2 40

#else
#define VOICE_PTR_INC 15 // we change to 10, around 1.25ms

#endif
// use ulaw domain !!
#define VOICE_LOAD_LEN 62



#define KEYS_NOKEY 0
#define KEYS_DEB 1
#define KEYS_WAIT_RELEASE 2
#define KEYS_WAIT_SYNC 3
#define KEYS_SYNCK_DELAY 4
#define KEYS_WAIT_OFF 5

// deb 15 means 30ms
#if SIMULATION
#define KEY_DEB_TIME 3
#else
#define KEY_DEB_TIME 15
#endif

#define SYS_IDLE 0
#define SYS_DIAL 1
#define SYS_RING 2 
#define SYS_SYNC 3

// PIOA 3,4,5 is LED,
// also PIOB 5,6

#define PADIR_DEFAULT 0xff

#define PA_PA7PWM 0x80
#if PAKEY345

#define PA_LEDR 0x01 // LEDB means ring,key,busytone led
#define PA_LEDR_INV 0xFe
#define PA_LEDL 0x02 // backlight, talk 20, ring, key 10s
#define PA_LEDL_INV 0xfd
#define PA_LEDADJ1 0X04
#define PA_LEDADJ1_INV 0xfb
#define PA_LEDADJ2 0X40
#define PA_LEDADJ2_INV 0xbf

#define PA_LED_ALL 0x07
#define PA_KEYS 0x38
#define PA_KEYS_INV 0xc7
#define PA_KEYS_R1 0x08
#define PA_KEYS_R2 0x10
#define PA_KEYS_R3 0x20

#define PA_ONOFF_ROW 0x10


#else

#define PA_LEDR 0x08 // LEDB means ring,key,busytone led
#define PA_LEDR_INV 0xF7
#define PA_LEDL 0x10 // backlight, talk 20, ring, key 10s
#define PA_LEDL_INV 0xef
#define PA_LEDADJ1 0X20
#define PA_LEDADJ1_INV 0xdf
#define PA_LEDADJ2 0X40
#define PA_LEDADJ2_INV 0xbf
#define PA_LED_ALL 0x78
#define PA_KEYS 0x7
#define PA_KEYS_INV 0xf8
#define PA_KEYS_R1 0x01
#define PA_KEYS_R2 0x02
#define PA_KEYS_R3 0x04


#define PA_ONOFF_ROW 0x02

#endif
// change to 2ms timer
#define TMAH_RELOAD ((unsigned char)(256-16))


#define PLAY_BUFAH 0x86
// now rec have 512 bytes
#define REC_STARTA 0x8400
#define REC_BUFAH 0x84
#define REC_BUFAHDBG 0x84
// length no longer than 64
#define EC_BUFO 0x8380
#define EC_BUFC 0x8300


// WHEN RECORDING, LINEAR DATA IS BUFFED TO ULAW FIRST
// T/R DATA ARE uChar BASIS
// RECORDING LINEAR BASIS IS PROCESSED LOCALLY
// SINCE WE HAVE RAM NOT USED YET.

// we try record/play ULAW directly, not need
// linear convert ... unless we need to do TDD noise rej
//#define REC_BUFUAH 3 


// PIOB5 is RF INT PIN if no DMI, if DMI, PIOA6 is the pin, it is digital, no jamming issue
#ifndef GIO1INV
#error "GIO1INV MUST BE DEFINED"
#else

#if GIO1INV
#define RFINTPIN (!(PIOB&0x20))
#else
#define RFINTPIN (PIOB&0x20)
#endif
#endif

// 3hz need 165ms toggle ledp a time, we take 4ms*42

#define KEY_CODE_SYNC 16
#define KEY_CODE_HANGUP 3
#define KEY_CODE_ONOFF 17
#define KEY_CODE_TALK 1
#define KEY_CODE_UP 13
#define KEY_CODE_DN 15
#define KEY_CODE_0 14
#define KEY_CODE_1 4
#define KEY_CODE_2 5
#define KEY_CODE_3 6
#define KEY_CODE_4 7
#define KEY_CODE_5 8
#define KEY_CODE_6 9
#define KEY_CODE_7 10
#define KEY_CODE_8 11
#define KEY_CODE_9 12
#define KEY_CODE_HF 2
#define KEY_CODE_TEST 18



// RF parameters


#define cslow() PIOC=0
#define cshigh() PIOC=1


// 2018 trying massive channel hopping
// if per T/R is 3ms, 
// 75 channel need 150ms, it is not good for power saving.
// so we reduce the possible channels
// and change to 2 phase, 1 phase is wakeup, second phase is answer
// if per tx is 0.5ms, 32 channel is 16ms, double is 32 ms.
// 2second 32ms is a better solution

// that is, phase1 need wakeup all handsets, 
// phase2 the handset shall answer
// if handset sleep 1 second, phase1 need 3 seconds
// if handset sleep 2 second, phase1 need 6 seconds that is a problem



// WAIT-CALL AND DIAL WAIT ACK
// WHEN DIALING, THE DIALER WILL SEND PACKET AND DELAY DIAL_WAKT_ACK TIME
// OTHERS WAKE UP "WAIT CALL TIME"
// WAIT CALL TIME < DIAL WAIT ACK * HOP_NUM


#if (SIMULATION)
#define KEY_BEEP_LEN 10
#else
#define KEY_BEEP_LEN 50
#endif


#define RINGPULSELEN 12
#define DIALLEN 160
#define RINGINTER 50
#define MISSMAX 30

#define MLD10 10

// progress uChar
#define PDIALING 0X55
#define PANSWERING 0xaa

#define SYNC_CHAN 40
#define SYNC_ID 55

// wait call time change to 5=12~14ms
#define WAIT_CALL_TIME 10

#define SPIMACRO 1

#define POWERON_LOUD 1

#if LINEAR_REC
#define RBUF16LEN 0x20
#else
#define RBUF16LEN 0x10
#endif


#endif

	.include "dmi.def"
	.DEFINE REDUCE_TDD 1
	.DEFINE REC_LINEAR 1  ; change 2 places
	.ifdef DMI
	.DEFINE TDDPH 0
	.DEFINE TDDPL 1
	.DEFINE TDDNH 0xff
	.DEFINE TDDNL 0xff
	.else
	.DEFINE TDDPH 0
	.DEFINE TDDPL 4
	.DEFINE TDDNH 0xff
	.DEFINE TDDNL 0xfc
	.endif
	; qe check, this file is bit 6 of status
SPICMD=2
SPIDAT1=3
SPIDAT2=4
TMP00=5;
	CALL	RESUME
	CALL	GET_STAT
	LDA	SPIDAT1
	STA	TMP00
	CALL	GET_STAT2
	LDA	SPIDAT1
	STA	SPIDAT2
	AND	#2		; new standard is bit 10
	jz	NEED_SETQE ; this version check bit 6
	JMP	QE_FINISH
NEED_SETQE:
	LDA	#2
	ORA	SPIDAT2
	STA	SPIDAT2
	LDA	#1
	STA	_SPIOP
	CALL	WR_STAT
	CALL	WAIT_BUSY
	JMP	QE_FINISH
WAIT_BUSY:
	CALL	GET_STAT
	LDA	SPIDAT1
	SHR
	JC	WAIT_BUSY
	RET
	
WR_STAT:
	LDA	#1
	STA	SPICMD
	CALL	SPIDMA2
	CALL	WAIT_BUSY
	JMP	QE_FINISH
RESUME:
	LDA	#0x7A
	STA	SPICMD
	CLRA	
	JMP	SPIDMA_CONT
GET_STAT2:
	LDA	#0x35
	STA	SPICMD
	JMP	SPIDMA
GET_STAT:
	LDA	#5
	STA	SPICMD
	JMP	SPIDMA
SPIDMA2:
	LDA	#2
	JMP	SPIDMA_CONT
SPIDMA:
	LDA	#1
SPIDMA_CONT:
	STA	_SDMALEN
	CLRA
	STA	(_SDMALEN+1)
	LDA	#SPICMD
	STA	_SDMAAL
	CLRA
	STA	_SDMAAH
	LDA	#5
	STA	_SPIDMAC
WAITDMA:
	LDA	_SPIDMAC
	ROL
	JC	WAITDMA
	RET
	
	
	
	
;QEDUMMY:	.DS 64
QE_FINISH:
	; load 100,200,300 to 200,300,400
	LDA	#4 ; unit is half page
	STA	_SPIOPRAH
	CLRA
	STA	_SPIH
	STA	_SPIL
	LDA	#1
	STA	_SPIM
	lda	#0x08
	STA	_SPIOP

	;LDA	#6
	;sta	_SPIOPRAH
	;LDA	#2
	;STA	_SPIM
	;lda	#0x08
	;STA	_SPIOP

	LDA	#3
	STA	_PRG_RAM
	JMP	CONT000

.ifdef REC_LINEAR
; **** linear rec always reduce TDD
; but for DMIC, the process 
; simplifies  to gain and L2U

.ifdef DMI 

_reduce_tdd:
	; we have no local here, no interrupt!!
	CRIT
	PUSHP1L
	PUSHP1H
	LDA _micSoftGainNow ; gain 2db/6db
	STA _MULBL
	LDA #2
	STA _MULSHIFT
	CLRA
	STA	_PTRCL
	STA _MULBH
	// tx data from 0x8101~0x8101+63
	// 8135 is the new data
	LDA #35   ; // 64 byte buf, update final 30 bytes, total 65 bytes
	STA _RAMP1L
	LDA #0x81
	STA _RAMP1H ; RAMP1->8135
_00762_DS1:
	LDA _ROMPH
	AND #0x85		; only 84,85 86->84
	STA _ROMPH




	
	LDA	@_ROMPINC ; load voiceiptr+ptrc

	STA   _MULAL 

	LDA @_ROMPINC

	STA	  _MULAH ; gain out

/*
	LDA _headNow
	AND #3
	JNZ  normal_rec
	LDA _rand8
	JNZ no_need_rst_rand8
	LDA #0xAC
	STA _rand8
no_need_rst_rand8:	

	LDA _rand8 
	SHR
	STA _MULAL
	JNC NO_XOR
	XOR #0xB8
	STA _rand8
	LDA #0xFF
	JMP cont_rnd
NO_XOR:
	STA _rand8
	CLRA
cont_rnd:	
	STA _MULAH
normal_rec:
*/
	LDA (_MULO+1)
	STA (_L2UBUF+0)
	LDA (_MULO+2)
	STA (_L2UBUF+1)
	LDA	_ULAWD
	STA	@_RAMP1INC
	LDA	_PTRCL
	INCA	
	STA	_PTRCL
	ADD	#226	; this is 256-30 or 256-20 !! VOICE_PTR_INC!!
	JNC	_00762_DS1	
	CLRA 
	STA _MULSHIFT
	POPP1H
	POPP1L
	NOCRIT
	RET


.else


; following is tdd reduce
; ROMPLH : voiceoptr 
; HWPALH 0:tdd, for calculation
_reduce_tdd:
	; we have no local here, no interrupt!!
	PUSHP1L
	PUSHP1H
	LDA _micSoftGainNow ; gain 2db/6db
	STA _MULBL
	LDA #2
	STA _MULSHIFT
	CLRA
	STA	_PTRCL
	STA _MULBH
	// tx data from 0x8101~0x8101+63
	// 8135 is the new data
	LDA #35   ; // 64 byte buf, update final 30 bytes, total 65 bytes
	STA _RAMP1L
	LDA #0x81
	STA _RAMP1H ; RAMP1->8135
_00762_DS1:
	LDA	#0x01
	STA	_HWPSEL
	LDA _ROMPH
	AND #0x85		; only 84,85 86->84
	STA _ROMPH
	
	SETB	_C
	LDA	@_ROMPINC ; load voiceiptr+ptrc
	SUBB	_HWDINC ; sub tdd

	STA   _MULAL 

	LDA @_ROMPINC
	SUBB	_HWDINC

	STA	  _MULAH ; gain out

	JMI	_00747_DS1
	CLRA
	STA	_HWPSEL
	LDA	_HWD
	ADD #TDDPL ; add1->7 ; tdd adj
	STA	_HWDINC
	LDA  #TDDPH
	ADDC	_HWD
	STA	_HWDINC
	JMP	_00748_DS1
_00747_DS1:
	CLRA
	STA	_HWPSEL
	LDA	_HWD
	ADD #TDDNL ; -1 -> -7
	STA	_HWDINC
	LDA	#TDDNH
	ADDC	_HWD
	STA	_HWDINC
_00748_DS1:
	LDA (_MULO+1)
	STA (_L2UBUF+0)
	LDA (_MULO+2)
	STA (_L2UBUF+1)
	LDA	_ULAWD
	STA	@_RAMP1INC
	LDA	_PTRCL
	INCA	
	STA	_PTRCL
	ADD	#226	; this is 256-30 or 256-20 !! VOICE_PTR_INC!!
	JNC	_00762_DS1	
	CLRA 
	STA _MULSHIFT
	POPP1H
	POPP1L
	RET
.endif

;--------------------------------------------------------
	.FUNC _prepare_voice_tx:$PNUM 0
;--------------------------------------------------------
;	.line	1366; "ms326a7137.c"	voice_ptru=RDMAL;
; if linear, we use another 100~11D as the ulaw buffer !! the code is not used anymore!!

	; change to add gain 
_prepare_voice_tx:	;Function start

; use next one
	LDA _RDMAH
	ROR 
	LDA _RDMAL   ; consider 84fe->8500, it may be read 8400
	ROR 
	jz  _prepare_voice_tx ; possibility low, 
;	;.line	1367; "ms326a7137.c"	voiceo_ptr=voice_ptru-VOICE_LOAD_LEN; // ulaw domain, overflow is ok
	STA	_voice_ptru ; 
	ADD	#0xc2
;	;.line	1371; "ms326a7137.c"	ROMPL=voiceo_ptr;
	STA	_voiceo_ptr
	LDA _voice_ptru
	ADD #226
	; we need update final VOICE_PTR_INC (30) 
	SHL 
	STA	_ROMPL
	CLRA
	ADDC #0x84			; this is not pic
	STA _ROMPH
;	;.line	1372; "ms326a7137.c"	HWPSEL=0;
	.ifndef DMI
	CLRA	
	STA	_HWPSEL
;	;.line	1373; "ms326a7137.c"	HWPALH=(char*)&tdd;
	LDA	#high (_tdd + 0)
	STA	(_HWPALH + 1)
	LDA	#(_tdd + 0)
	STA	_HWPALH
;	;.line	1374; "ms326a7137.c"	HWPSEL=1; //high hwptr change to 8100
	LDA	#0x01
	STA	_HWPSEL
;	;.line	1376; "ms326a7137.c"	..asm
	LDA	#high (_tdd + 0)
;	;.line	1377; "ms326a7137.c"	..asm
	STA	(_HWPALH + 1)
;	;.line	1378; "ms326a7137.c"	..asm
	LDA	#(_tdd + 0)
;	;.line	1379; "ms326a7137.c"	..asm
	STA	_HWPALH
	.endif
	call	_reduce_tdd; always reduce tdd
;NOTDDYET:
;	;.line	1381; "ms326a7137.c"	 ..asm
;	;.line	1413; "ms326a7137.c"	cslow();
	CLRA
	STA  _PIOC
	STA   _ROMPL
	LDA   #0x81
	STA   _ROMPH
	
;	;.line	1414; "ms326a7137.c"	SPIDAT=FIFO_REG; // voice always no ack
	LDA	#0x05
	STA	@_ROMPINC ; 0x8100 = 5
;	;.line	1415; "ms326a7137.c"	SPIDAT=TAG_VOICE; // first byte is VOICE-TAG!!
	LDA	_headNow;
	STA	@_ROMPINC ; 0x8101, start payload, headnow
;	;.line	1416; "ms326a7137.c"	SPIDAT=voiceo_ptr; // this is byte basis!! 64-2=62
	LDA	_voiceo_ptr
	STA	@_ROMPINC ; 0x8102, voiceo ptr , others should start at 0x8103?
;	;.line	1419; "ms326a7137.c"	SDMAAH=REC_BUFAHDBG
	LDA	#0x81 ; // ref ram addr?
	STA	_SDMAAH
;	;.line	1420; "ms326a7137.c"	SDMAAL=voiceo_ptr;
	;LDA	_voiceo_ptr
	CLRA 
;	;.line	1432; "ms326a7137.c"	if(voiceo_ptr<257-VOICE_LOAD_LEN) // only 1 time transfer
; only single 
	STA	_SDMAAL
;	ADD	#0x3d
;	JC	_00825_DS1
;	;.line	1434; "ms326a7137.c"	SDMALEN=VOICE_LOAD_LEN-1;
	LDA	#64   
	STA	_SDMALEN
	CLRA	
	STA	(_SDMALEN + 1)
;	;.line	1435; "ms326a7137.c"	SPIDMAC=0x02; // write only
	LDA	#0x02
	STA	_SPIDMAC
_00815_DS1:
;	;.line	1436; "ms326a7137.c"	while(SPIDMAC&0x80);
	LDA	_SPIDMAC
	JMI	_00815_DS1
;	;.line	1451; "ms326a7137.c"	cshigh();
	LDA	#0x01
	STA	_PIOC

	; direct send
	CLRA
	STA _PIOC
	LDA #0xD0; // TX 
	STA _SPIDAT
	LDA #0x01
	STA _PIOC 
	RET
	// after prepare_voice_tx, ROMPTR at 0x8103
	.ENDFUNC _prepare_voice_tx	

	.FUNC _shift_voice_buf:$PNUM 0
_shift_voice_buf:
	; SHIFT output buf  ; now romptr at 0x8103 we need set hwp to 0x8100 +33
	CLRA 
	STA  _HWPSEL
	LDA  #33
	STA  _HWPALH
	LDA  #0x81
	STA  (_HWPALH+1) ; // dest is 0x100
LOOP_MOV:
	LDA  _HWDINC
	STA  @_ROMPINC
	LDA  _HWPALH
	ADD  #191     ; 256-65 = 191
	JNC  LOOP_MOV

;	;.line	1453; "ms326a7137.c"	DMA_IL=voiceo_ptr+VOICE_PTR_INC+(PAYLOADLEN-1);-- it is 8 bit , 16 bit need shift
	LDA	#92 	; this is 30+62=92 or 20+62=82, depends on INC
	ADD	_voiceo_ptr
	SHL    ; 
	STA	_DMA_IL
;	;.line	1454; "ms326a7137.c"	GIF=0x1f; // always clear flags
	LDA	#0x1f
	STA	_GIF
	RET	
	.ENDFUNC _shift_voice_buf
; exit point of _prepare_voice_tx
	



.else
.ifdef REDUCE_TDD
; following is tdd reduce
_reduce_tdd:
	CLRA
	STA	_PTRCL
_00762_DS1:
	LDA	#0x84
	STA	_ROMPH
	LDA	@_ROMP
	STA	_ULAWD
	LDA	#0x01
	STA	_HWPSEL
	
	SETB	_C
	LDA	_L2UBUF
	SUBB	_HWDINC
	STA	_L2UBUF
	LDA	(_L2UBUF + 1)
	SUBB	_HWDINC
	STA	(_L2UBUF + 1)
	JMI	_00747_DS1
	CLRA
	STA	_HWPSEL
	LDA	_HWD
	ADD #2 ; add1->7
	STA	_HWDINC
	CLRA	
	ADDC	_HWD
	STA	_HWDINC
	JMP	_00748_DS1
_00747_DS1:
	CLRA
	STA	_HWPSEL
	LDA	_HWD
	ADD #0xFE ; -1 -> -7
	STA	_HWDINC
	LDA	#0xff
	ADDC	_HWD
	STA	_HWDINC
_00748_DS1:
	LDA	_ULAWD
	STA	@_ROMPINC
	LDA	_PTRCL
	INCA	
	STA	_PTRCL
	ADD	#226	; this is 256-30 or 256-20 !! VOICE_PTR_INC!!
	JNC	_00762_DS1	
	RET
.endif	

;--------------------------------------------------------
	.FUNC _prepare_voice_tx:$PNUM 0
;--------------------------------------------------------
;	.line	1366; "ms326a7137.c"	voice_ptru=RDMAL;
_prepare_voice_tx:	;Function start

; use next one

;	LDA	_RDMAL
;	STA _PTRCL
;LOOP_CHK:
;	LDA _RDMAL
;	XOR _PTRCL
;	JZ  LOOP_CHK ; random 6 clock, should be ok for TDD
	LDA _RDMAL
;	;.line	1367; "ms326a7137.c"	voiceo_ptr=voice_ptru-VOICE_LOAD_LEN; // ulaw domain, overflow is ok
	STA	_voice_ptru
	ADD	#0xc2
;	;.line	1371; "ms326a7137.c"	ROMPL=voiceo_ptr;
	STA	_voiceo_ptr
	STA	_ROMPL
;	;.line	1372; "ms326a7137.c"	HWPSEL=0;
	CLRA	
	STA	_HWPSEL
;	;.line	1373; "ms326a7137.c"	HWPALH=(char*)&tdd;
	LDA	#high (_tdd + 0)
	STA	(_HWPALH + 1)
	LDA	#(_tdd + 0)
	STA	_HWPALH
;	;.line	1374; "ms326a7137.c"	HWPSEL=1;
	LDA	#0x01
	STA	_HWPSEL
;	;.line	1376; "ms326a7137.c"	..asm
	LDA	#high (_tdd + 0)
;	;.line	1377; "ms326a7137.c"	..asm
	STA	(_HWPALH + 1)
;	;.line	1378; "ms326a7137.c"	..asm
	LDA	#(_tdd + 0)
;	;.line	1379; "ms326a7137.c"	..asm
	STA	_HWPALH
;	;.line	1380; "ms326a7137.c"	..asm
	.ifdef REDUCE_TDD
	LDA	_FILTERGP ; 2020 change to filtergp
	
	JZ	NOTDDYET
	call	_reduce_tdd
	.endif
NOTDDYET:
;	;.line	1381; "ms326a7137.c"	 ..asm
	
;	;.line	1413; "ms326a7137.c"	cslow();
	CLRA	
	STA	_PIOC
;	;.line	1414; "ms326a7137.c"	SPIDAT=FIFO_REG; // voice always no ack
	LDA	#0x05
	STA	_SPIDAT
;	;.line	1415; "ms326a7137.c"	SPIDAT=TAG_VOICE; // first byte is VOICE-TAG!!
	LDA	_headNow;
	STA	_SPIDAT
;	;.line	1416; "ms326a7137.c"	SPIDAT=voiceo_ptr; // this is byte basis!! 64-2=62
	LDA	_voiceo_ptr
	STA	_SPIDAT
;	;.line	1419; "ms326a7137.c"	SDMAAH=REC_BUFAHDBG;
	LDA	#0x84
	STA	_SDMAAH
;	;.line	1420; "ms326a7137.c"	SDMAAL=voiceo_ptr;
	LDA	_voiceo_ptr
;	;.line	1432; "ms326a7137.c"	if(voiceo_ptr<257-VOICE_LOAD_LEN) // only 1 time transfer
	STA	_SDMAAL
	ADD	#0x3d
	JC	_00825_DS1
;	;.line	1434; "ms326a7137.c"	SDMALEN=VOICE_LOAD_LEN-1;
	LDA	#0x3d
	STA	_SDMALEN
	CLRA	
	STA	(_SDMALEN + 1)
;	;.line	1435; "ms326a7137.c"	SPIDMAC=0x02; // write only
	LDA	#0x02
	STA	_SPIDMAC
_00815_DS1:
;	;.line	1436; "ms326a7137.c"	while(SPIDMAC&0x80);
	LDA	_SPIDMAC
	JMI	_00815_DS1
	JMP	_00826_DS1
_00825_DS1:
;	;.line	1438; "ms326a7137.c"	len1 = 256-voiceo_ptr;
	LDA	_voiceo_ptr
	SETB	_C
	CLRA	
	SUBB	_voiceo_ptr
;	;.line	1439; "ms326a7137.c"	SDMALEN=len1-1;
	STA	_PTRCL
	DECA	
	STA	_SDMALEN
	CLRA	
	ADDC	#0xff
	STA	(_SDMALEN + 1)
;	;.line	1440; "ms326a7137.c"	SPIDMAC=0x02;
	LDA	#0x02
	STA	_SPIDMAC
_00818_DS1:
;	;.line	1441; "ms326a7137.c"	while(SPIDMAC&0x80);
	LDA	_SPIDMAC
	JMI	_00818_DS1
;	;.line	1443; "ms326a7137.c"	SDMALEN=VOICE_LOAD_LEN-len1-1;
	SETB	_C
	LDA	#0x3d
	SUBB	_PTRCL
	STA	_SDMALEN
	CLRA	
	SUBB	#0x00
	STA	(_SDMALEN + 1)
;	;.line	1444; "ms326a7137.c"	SDMAAH=REC_BUFAHDBG;
	LDA	#0x84
	STA	_SDMAAH
;	;.line	1445; "ms326a7137.c"	SDMAAL=0;
	CLRA	
	STA	_SDMAAL
;	;.line	1446; "ms326a7137.c"	SPIDMAC=2;
	LDA	#0x02
	STA	_SPIDMAC
_00821_DS1:
;	;.line	1447; "ms326a7137.c"	while(SPIDMAC&0x80);
	LDA	_SPIDMAC
	JMI	_00821_DS1
_00826_DS1:
;	;.line	1451; "ms326a7137.c"	cshigh();
	LDA	#0x01
	STA	_PIOC
;	;.line	1453; "ms326a7137.c"	DMA_IL=voiceo_ptr+VOICE_PTR_INC+(PAYLOADLEN-1);
	LDA	#92 	; this is 30+62=92 or 20+62=82, depends on INC
	ADD	_voiceo_ptr
	STA	_DMA_IL
;	;.line	1454; "ms326a7137.c"	GIF=0x1f; // always clear flags
	LDA	#0x1f
	STA	_GIF
	RET	
; exit point of _prepare_voice_tx
	.ENDFUNC _prepare_voice_tx	
.endif 	
;--------------------------------------------------------
	.FUNC _adjHeadNow:$PNUM 0
;--------------------------------------------------------
_adjHeadNow:	; Function start
	; adjust _headNow by the energy of recorded voice
	; sometimes the tdd is loud
	; dmi signal is very small, if RECPWR+2 has anyvalue, it should be scale down immediately
	.ifdef DMI
	; 2020 CHANGE, not decide headnow directly, filted
	; the recpwr will be update every 60 samples due to DMA_IL moving
	; > 2^19 is loud
	   LDA (_RECPWR+2)
	   JZ  rec_not_loud
	   
	   LDA #0x36
	   STA _headNow 
	   ret
rec_not_loud:
       LDA _RECPWR
	   AND #0x80
	   ORA (_RECPWR+1) ; dmic is small.. 
	   JZ rec_not_middle
	   LDA #0x35
	   STA _headNow 
	   ret

rec_not_middle:
	  LDA #0x34
	  STA _headNow
	  RET	  

	.else
	   LDA (_RECPWR+3)
	   ADD #250
	   JNC  rec_not_loud
	   LDA #0x36
	   STA _headNow
	   ret
rec_not_loud:	
	   LDA (_RECPWR+3)
	   JNZ rec_middle
	   LDA (_RECPWR+2)
	   JZ  rec_not_middle
rec_middle:
	   LDA #0x35
	   STA _headNow
	   RET
rec_not_middle:
	   LDA #0x34
	   STA _headNow
	   RET
	
	  
	.endif

	.ENDFUNC _adjHeadNow


;--------------------------------------------------------
	.FUNC _adjLevel:$PNUM 1
;--------------------------------------------------------
;	.line	698; "ms233a7196.c"	void adjLevel(unsigned char tag)
_adjLevel:	;Function start
	STA	_temp001
;	;.line	700; "ms233a7196.c"	if (handFree)
	LDA	_handFree
	JZ	_00346_DS_qe
;	;.line	703; "ms233a7196.c"	if (tag == 0)
	LDA	_temp001
	JNZ	_00325_DS_qe
;	;.line	705; "ms233a7196.c"	++lowCount;
	LDA	_lowCount
	INCA	
	STA	_lowCount
;	;.line	706; "ms233a7196.c"	if (lowCount > 20)
	SETB	_C
	LDA	#0x14
	SUBB	_lowCount
	JC	_00316_DS_qe
;	;.line	708; "ms233a7196.c"	if (FILTERGP > INI_FGP)
	SETB	_C
	LDA	#0x05
	SUBB	_FILTERGP
	JC	_00314_DS_qe
;	;.line	709; "ms233a7196.c"	FILTERGP--;
	LDA	_FILTERGP
	DECA	
	STA	_FILTERGP
_00314_DS_qe:
;	;.line	710; "ms233a7196.c"	lowCount = 0;
	CLRA	
	STA	_lowCount
_00316_DS_qe:
;	;.line	712; "ms233a7196.c"	highCount = 0;
	CLRA	
	STA	_highCount
	JMP	_00326_DS_qe
_00325_DS_qe:
;	;.line	716; "ms233a7196.c"	++highCount;
	LDA	_highCount
	INCA	
	STA	_highCount
;	;.line	717; "ms233a7196.c"	if (highCount > 10)
	SETB	_C
	LDA	#0x0a
	SUBB	_highCount
	JC	_00323_DS_qe
;	;.line	719; "ms233a7196.c"	if (FILTERGP < targetFGP)
	SETB	_C
	LDA	_FILTERGP
	SUBB	_targetFGP
	JC	_00321_DS_qe
;	;.line	721; "ms233a7196.c"	unsigned char k = FILTERGP + 10; // loud faster
	LDA	_FILTERGP
	ADD	#0x0a
;	;.line	722; "ms233a7196.c"	if (k < targetFGP)
	STA	_temp000
	SETB	_C
	SUBB	_targetFGP
	JC	_00318_DS_qe
;	;.line	723; "ms233a7196.c"	FILTERGP = k;
	LDA	_temp000
	STA	_FILTERGP
	JMP	_00321_DS_qe
_00318_DS_qe:
;	;.line	726; "ms233a7196.c"	FILTERGP = targetFGP;
	LDA	_targetFGP
	STA	_FILTERGP
_00321_DS_qe:
;	;.line	729; "ms233a7196.c"	highCount = 0;
	CLRA	
	STA	_highCount
_00323_DS_qe:
;	;.line	731; "ms233a7196.c"	lowCount = 0;
	CLRA	
	STA	_lowCount
_00326_DS_qe:
;	;.line	734; "ms233a7196.c"	if (tag == 3)
	LDA	_temp001
	XOR	#0x03
	JNZ	_00342_DS_qe
;	;.line	737; "ms233a7196.c"	micSoftGainNow = 40; // small gain, train slowly
	LDA	#0x28
	STA	_micSoftGainNow
	JMP	_00346_DS_qe
_00342_DS_qe:
;	;.line	742; "ms233a7196.c"	else if (tag >= 2) // if other side loud, we quiet
	LDA	_temp001
	ADD	#0xfe
	JNC	_00339_DS_qe
;	;.line	745; "ms233a7196.c"	if (micSoftGainNow > 30) // to very small
	SETB	_C
	LDA	#0x1e
	SUBB	_micSoftGainNow
	JC	_00346_DS_qe
;	;.line	746; "ms233a7196.c"	micSoftGainNow = (micSoftGainNow * 250) >> 8;
	LDA	_micSoftGainNow
	ADD #0xFA; -=6
	STA	_micSoftGainNow
	JMP	_00346_DS_qe
_00339_DS_qe:
;	;.line	752; "ms233a7196.c"	else if (tag == 0)
	LDA	_temp001
	JNZ	_00336_DS_qe
;	;.line	755; "ms233a7196.c"	if (micSoftGainNow < DMIGAINHF)
	LDA	_micSoftGainNow
	ADD	#0x4c
	JC	_00346_DS_qe
;	;.line	756; "ms233a7196.c"	micSoftGainNow++;
	LDA	_micSoftGainNow
	INCA	
	STA	_micSoftGainNow
	JMP	_00346_DS_qe
_00336_DS_qe:
;	;.line	764; "ms233a7196.c"	if (++middleCount >= 20)
	LDA	_middleCount
	INCA	
	STA	_middleCount
	ADD	#0xec
	JNC	_00346_DS_qe
;	;.line	767; "ms233a7196.c"	if (micSoftGainNow < DMIGAINHF)
	LDA	_micSoftGainNow
	ADD	#0x4c
	JC	_00346_DS_qe
;	;.line	768; "ms233a7196.c"	micSoftGainNow++;
	LDA	_micSoftGainNow
	INCA	
	STA	_micSoftGainNow
_00346_DS_qe:
;	;.line	776; "ms233a7196.c"	}
	RET	
; exit point of _adjLevel
	.ENDFUNC _adjLevel
DUMMY: .DS 32
CONT000:
	

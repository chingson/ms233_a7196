

2018 : change to a7196 (2MBPS)

2020:

Features:
    1. AMIC A7196 19DBM RF transceiver connected.
    2. Full duplex talk mode.
    3. Hand-free support.
    4. keypad option to PIOA345 connect to PIOB0~4
    5. Digital Microphone support (PDM format)

Connection:
    KEY Matrix: PIOA012 or PIOA345 connect PIOB 0~4
    PIOB5: RF GIO1
    PIOB6: reserved.
    PIOB7: RF clk 16MHZ out
    PIOA345/012: LED RING/Backlight/HF
    PIOA6: digital mic clock
    PIOA7: PWM for 32 OHM handset speaker.
    SPKPN: Class D amplifier for 8 OHM speaker.

    MICIN: analog mic signal or Digital mic data signal

    PIOC0: CS of RF
    PIOC1: CK of RF
    PIOC2: MOSI of RF
    PIOC3: MISO (GIO2)

